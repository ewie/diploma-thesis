\chapter{Prototype Implementation}
\label{chap:implementation}

This chapter presents a prototype implementation based on the solution outlined in \cref{chap:planner,chap:execution}.
This prototype consist of three parts:
\begin{ienumerate}
  \item a library providing the mashup planner,
  \item a web service making the planner available to clients, and
  \item a widget consuming the web service, composing mashups and conducting mashup execution.
\end{ienumerate}
The prototype is free software~(BSD 3-Clause License) and is publicly available at \url{https://github.com/ewie/cobalt}.



\input{tex/chap/implementation/library}
\input{tex/chap/implementation/service}
\input{tex/chap/implementation/client}



\summary

Based on the proposed solution, a prototype implementation consisting of a planner library, web service, and composer widget was developed.
The library is designed to be reused in other software and provides the planning algorithm implementation in Java.
The planning graph is implemented such that widget instance information is made explicit instead of implicit information requiring plan search to determine widget instances.
Actions are discovered via a repository separate from the actual planning logic.
The repository allows different representations of widget descriptions.
An ontology to describe widgets and mashups in terms of functionalities, is used as the basis for the default repository implementation using Apache Jena to store and process semantic data.
The ontology offers concepts to describe the properties required by the planning algorithm~(\eg preconditions and effects) as well as the execution-specific aspects~(\ie instructions).

The web service exposes an endpoint for consuming the planning algorithm via \gls{HTTP} using a server realized with Jetty.
Client-server communication uses \gls{JSON} for data serialization to allow the native processing of data in JavaScript.
The structure of request and response data is properly defined using \gls{JSON}~Schema, including a schema for plans produced by the planning algorithm.
Responses of the planning service can optionally use \gls{JSON} Pointers to reduce data caused by repeating structures.
This also allows to easily process plan data because objects can be compared based on identity rather than value to check if two objects refer to the same concept~(\eg widget or instruction).

Users interact with the composer widget to state the intended mashup functionalities and consume the web service.
The functionalities are given in form of a semantic description of the desired mashup specifying all required functionalities.
Requests allow the semantic description to be formatted as \gls{Turtle}, \gls{RDF}/\gls{XML}, or \gls{RDF}/\gls{JSON}.
The second and third format is mainly intended for machine-based service consumers.

Resulting plans are rated and presented to users for selection in a separate dialogue.
Selecting a plan causes the composer widget to configure the respective mashup using Apache Rave's \gls{API}.
This tightly couples the composer widget to a specific mashup platform, Apache Rave in this case.
Users are assisted in executing mashups with a sequence of instructions provided by each widget's semantic description.
Instructions are divided into individual steps corresponding to individual planning graph levels.
In order to achieve mashup's intended functionalities, instructions must be followed as stated.
Based on publications, an action's corresponding instructions can be determined to be completed.
In this case the instructions are visually marked as done and users can proceed with remaining instructions.
Deviations from a planned execution cannot be avoided.
Their effects on the mashup through \gls{IWC}, however, can be prevented by configuring the execution environment as execution progresses.
Unexpected publications are ignored by modifying Apache Rave's publish-subscribe system to check each publication for permission based on the current execution step.
When allowed, publications are further processed by the publish-subscribe system's default logic.
This monitoring of publications also allows synchronizing instruction completion with mashup execution.
