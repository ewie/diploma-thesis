\section{Scope}

The existing research on mashups and their composition at the \gls{VSR}%
  \footnote{\AccessedURL{2014}{9}{22}{https:/vsr.informatik.tu-chemnitz.de/}}
provides the scope of this work.
Their research on mashups focuses on a specific widget model described in this section along with its use in mashup composition.



\subsection{Widget Model}
\label{sec:widget-model}

Widgets following the model used by the \gls{VSR}, hereafter referred to as \gls{VWM}, offer a set of actions realizing widget behaviour.%
  \footnote{%
    The model name is used to simplify reading.
    Despite that, this widget model has no official name.}
The internal state is represented by a set of typed properties.
Semantic concepts serve as types and also uniquely identify each property.
This has the drawback that conceptually distinct properties cannot share the same type as they would be identified as a single property.
The model also describes a widget's \gls{UI} in terms of user interactions to trigger specific actions.
Changes to properties are communicated to the environment through dedicated messages.
Each message is identified by the property it belongs to and carries the changed value as content.
Receiving data from other widgets is realized by subscribing to messages for respective own properties.
When a message is received, the associated property's value is replaced with the value given in the message content.
This way each widget can synchronize its internal state with other widgets through \gls{IWC}.
\here{widget-change-internal-state}%
It is assumed that only valid values are published or integrated into a receiving widget's state.
This ensures that other widget states do not get corrupted.
Publications about the absence of values~(\eg in the form of a null values) do not cause other widgets to delete values of their corresponding properties.
If this would be the case, a single widget could render a whole mashup useless because it would be able to effectively delete values from internal states of other widgets.
It is further assumed that widgets do not receive publications made by themselves~(by subscribing to messages it also publishes).
The widget model is illustrated in \cref{fig:widget-model}.

\begin{figure}
  \centering
  \includegraphics{fig/widget-model}
  \caption[Widget Model Forming the Basis of This Work]{%
    An illustration of interactions found in the \gls{VWM}.
    Users interact with mashups via individual widgets~(one shown in detail).
    Communication between widgets is realized via an event bus~(publish-subscribe).}
  \label{fig:widget-model}
\end{figure}

The messaging system is realized with the publish-subscribe pattern~\cite[sect.~3.9]{birman:1987}.
Each component, widgets in this case, can subscribe to relevant topics~(\eg messages matching a widget's own properties) and receive messages published by any other component on a specific topic.
A subscriber itself can also be a publisher.
Components do not need to have knowledge about one another and it is not required for a topic to have subscribers.
When a message has no subscribers it will be discarded.



\subsection{Mashup Composition}
\label{sec:mashup-composition}

When a mashup is loaded into an execution environment its widgets get initialized with some initial state defined by widget authors.
Because actions may require some state different from the initial state, widgets need to be transitioned into a state enabling the execution of required actions.
This state change can only be triggered by other actions of the same widget or by receiving messages from other widgets broadcasting changes made to their internal state.
This requires actions~(from any widget) to be performed before some other action can be invoked, leading to a chain of actions.
Allowing widgets to perform uncoordinated \gls{IWC} does not automatically create mashups with desired functionality when mashups are composed by simply adding heterogeneous widgets to the same environment.
This may cause two unintended behaviours within a mashup.

Firstly it can cause feedback loops~(\cref{fig:feedback-loop}).
Widget~\emph{A} publishes message~\emph{X} which causes widget~\emph{B} to publish message~\emph{Y}.
The loop is established when either~\emph{A} directly receives~\emph{Y} and again publishes~\emph{X} or via a third widget~\emph{C} publishing~\emph{Z} when receiving~\emph{Y}.
The third widget~\emph{C} illustrates that this loop does not have to be caused by a direct relation between widgets but may be created by a longer message cycle through an entire mashup.
Such a loop must not manifest itself every time~\emph{A} publishes~\emph{X}.
When~\emph{Y} gets published the loop may be caused only when~\emph{X} carries specific values.
But the pure possibility already requires the handling or prevention of eventual loops.
\name{Wilson} et al. state that the risk for this behaviour exists when mashups have no predefined plan~\cite[sect.~3.2]{wilson:2011}.

\begin{figure}
  \centering
  \includegraphics{fig/feedback-loop}
  \caption[Widgets Causing a Feedback Loop]{%
    Three widgets causing a feedback loop.
    Dashed elements illustrate an indirect feedback loop over an arbitrary number of intermediate widgets, in this case one widget~(C).
    Dotted arcs denote dependency relations between messages.}
  \label{fig:feedback-loop}
\end{figure}

The second behaviour to be avoided are messages which interfere with a mashup's intended functionality.
As described \where{widget-change-internal-state} the interaction with a widget will eventually change its internal state.
When these changes are published, they are picked up by other widgets causing them to update their internal state.
This could disrupt the execution progress in another widget where a certain functionality should be enabled~(by achieving a required internal state).
Because a widget requires to be in a specific state to perform certain actions, any changes to this state may disable the execution of actions as pre-conditions no longer hold true.
Because \gls{IWC} is uncoordinated it is possible for a user to unintentionally change the internal state of widget~\emph{A} by simply interacting with another widget~\emph{B}.
This interaction will eventually cause changes to the properties of~\emph{B} and thereby publications received by~\emph{A}~(given both widgets share a common property for which a value gets published).
Widget~\emph{A} will incorporate the received value into its internal state thereby making the widget unable to perform required actions.

This interference can be avoided by ensuring a certain functionality is achieved before invoking any interfering actions~(\cref{fig:execution-orders}, top).
This solution only works when widgets do not need to interact at all in order to achieve some functionality.
But it is more likely that widgets will require some interaction, especially when they need to share information in a scenario similar to the concurrent execution of threads: users have to alternate between widgets by performing one action in \emph{A}, switch to \emph{B} to perform another action, then switch back to \emph{A}, and so on.
Both widgets realize their functionality concurrently: the execution cannot be ordered such that either \emph{A} or \emph{B} completes its functionality before the other~(\cref{fig:execution-orders}, bottom).

\begin{figure}
  \centering
  \includegraphics{fig/execution-orders}
  \caption[Execution Orders of Widget Actions]{%
    Action execution order of widgets \emph{A} and \emph{B}.
    Subscripts are used to identify individual actions.
    The top shows the sequential execution of actions from independent widgets.
    The interleaved execution~(concurrent execution of widgets) required by their mutually dependency is depicted in the bottom.
    Execution goes from left to right.}
  \label{fig:execution-orders}
\end{figure}
