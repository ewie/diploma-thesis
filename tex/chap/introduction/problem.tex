\section{The Problem with Manual Mashup Composition and Execution}
\label{sec:problem-with-manual-composition}

The composition process can be divided into two steps:
\begin{ienumerate}
  \item users have to select suitable widgets and
  \item add them to a mashup environment in order to use resulting mashups.
\end{ienumerate}
Adding widgets, once selected, to an environment depends on a user's mashup platform and may not be simple, depending on the platform's quality in terms of usability.
Selecting the right widgets is the most crucial step, as it determines the resulting mashup's quality in terms of functionality and usability.
Because widgets are independent of one another to achieve a high degree of re-usability, there is a chance that more than one widget is suitable for a particular set of requirements.
Therefore, users must select those widgets most appropriate for fulfilling their needs.
The widget selection is a three step process.
\begin{enumerate}
  \item Users need to know the functions to be realized by widgets.
  \item Once this information is present, widgets realizing any of the required functions need to be discovered.
  \item After discovering, widgets with comparable functions need to be classified depending on how suitable they would be in solving the requirements.
\end{enumerate}

Assuming the end users have no technical skills, these three steps present major obstacles.
The first step, information about desired functionality, can only be accomplished by users, as only they know what should be achieved with an application.
One, however, must distinguish between the desired outcome and the actions necessary to accomplish said outcome.
Users may only know what to achieve but not how it can or must be achieved.
For this purpose a dialogue system is necessary.
This allows users to state their needs and refine them upon feedback by the system.

Discovering widgets based on functionality requires knowledge about each widget's functionality, \ie the functions it provides.
Machine-readable descriptions that may exist to support any automated processing of widgets, may be understood by skilled users~(especially professionals in the field, \eg researchers or developers) but are no useful source for average user.
For end users, with insufficient or no technical knowledge at all, the only source in this regard is a widget's verbal description.
Such a description may be found in the index of available widgets on a user's mashup platform.
Descriptions may exist in textual form~(other forms include graphics or video), outlining a widget's functionality, intended usage, and instructions on how to use it~(\eg user interactions).
Depending on a widget's complexity, understanding such a description may be a tedious task on its own, especially when there are multiple widgets available leading to an overload of information.
This presents two problems:
\begin{enumerate}
  \item Extracting information about widget functionalities.
  \item Classifying widgets depending on how suitable they would be in satisfying particular requirements.
\end{enumerate}
Comprehending a widget's description also depends on its quality.
Problems arise when the text is not provided in a user's native language~(or any language they are capable of), imposing a language barrier.
The quality is further impaired when the text, although readable, cannot be understood because of the used style~(technical terminology versus simple language).
This problem is not unusual for texts written by people lacking a user's perspective, \eg developers~\cite{samuals:2013}.
The text may also be incomplete, omitting explanations or crucial features.
This can happen when the author was not aware that users may require additional exposition by assuming a certain target audience.
The omission may be caused by overseeing aspects due to a widget's complexity or simply because of time pressure.
When the author is a single individual, privately developing widgets, quality control on documentation may fall short because it is not a central aspect.
Developments backed up by corporations can offer this quality control, especially for paying customers.

Because composition is the key aspect of mashups, widget authors will most likely rely on data dependencies to be resolved by mashup composers.
Instead of providing widgets with a \gls{UI} to provide data, authors may expect mashups to contain separate widgets providing necessary data.
This may create a dependency chain.
One widget requires another widget, requiring a third widget, and so on.
As a result, the mentioned three-step process must be repeated until no more unmet dependencies remain.

The information exchange between widgets gives rise to another problem: widget compatibility.
Widgets can be combined only when it is possible for them to exchange data.
For this to happen, they must share a convention on how to communicate information.
It requires agreement on both syntax and semantic.
The syntax is given by the data format used to exchange information, \ie how the data is formatted and how it must be processed to retrieve the encoded information.
Semantics, on the other hand, determine how information must be interpreted to make sense of it.
Incompatible widgets, \eg by using different syntaxes, cannot communicate at all or at least not productively.
They may, by chance, be able to exchange information because their data formats may share similarities.
However, they will not be able to evaluate the data with the same meaning the sender intended when semantics are incompatible.
Widget incompatibility is a key issue as widgets are likely to be heterogeneous in design when originating from different authors that may also develop separately from one another.

When a working mashup exists, only half the work is done.
The execution of mashups in order to make use of their intended functionality remains.
Because of mentioned information overload users may not know how to use certain widgets or what to do in particular.
Not all functions offered by widgets may be necessary to be performed in order to produce data required by other widgets or to realize a functionality.
The heterogeneous quality of widgets also applies to their \glspl{UI}.
Without further explanation on how to interact with widgets to invoke certain functions, their usage may not be apparent.

The \gls{UI} also presents the issue of accessibility.
\Gls{HCI} is an important aspect of application design in general and a field of ongoing research~\cite{carroll:2013}.
Despite that, users may have very specific preferences on what they can and cannot use in terms of \gls{IT}.
This can be personal characteristics like disabilities but also technical issues~\cite[chap.~1]{rutter:2006}, giving rise to the problem of accessibility.
Using widgets not compatible with user preferences results in mashups too hard to use or completely unusable for particular users.

The vast amount of information to understand as well as the time consuming, possibly repeating, process of discovery, selection, and widget combination present a burden too large for most people to bear.
Moreover, time is the most costly expenditure%
  \footnote{Credited to \name{Antiphon}~\cite[XXVIII.2]{plutarch:lives:antony:1920}.}
that needs to be cherished.
When people need software to solve problems, they want this software right away instead of waiting for it being developed.
