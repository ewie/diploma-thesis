\chapter{Concept for the Automated Composition and Execution of Mashups}
\label{chap:concept}

This chapter presents a concept for the automated composition and execution of mashups.
It provides an overview of the solution presented in detail in \cref{chap:planner,chap:execution}.
\Cref{fig:components} illustrates components and their instructions explained in this chapter.
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{fig/components}
  \caption[Components and Their Interactions]{Components and their interactions.}
  \label{fig:components}
\end{figure}

Users interact with a Composer Widget to automatically compose mashups.
The Composer Widget resides in an initially empty mashup~(\cf\cref{fig:components}~Mashup).
Users must manually create an empty mashup and add the Composer Widget.
The very same mashup will host widgets from resulting mashups~(\cf\cref{fig:components}~Application Widgets).
This is similar to the \gls{ACE} presented in~\cite{chowdhury:2013}~(\cf\cref{sec:complementary-assistance-mechanisms}).

Mashup composition is realized by a two-step process consisting of
\begin{ienumerate}
  \item mashup planning and
  \item mashup configuration.
\end{ienumerate}
The first step, mashup planning, is realized by a Planning Service~(\cf\cref{fig:components}~Planning Service) separate from the users mashup platform.
This separation allows the planning to be independent from specific mashup platforms.
The Planning Service is responsible for finding mashup plans for some goal mashup specified via the Composer Widget.
Goal mashups are defined in terms of the functionality to realize.
Mashup plans are sequences of actions to be performed on a set of widgets to realize goal functionalities.
The mashup planning algorithm is explained in detail in \cref{chap:planner}.
The planner discovers widgets via a knowledge base which has access to widget descriptors providing information about widget interfaces necessary for planning.
Widget descriptors are separate from a mashup platforms own widget repository.
It is required that all widget descriptors have corresponding widgets registered with the user's mashup platform.

When planning is successful a set of plans will be returned to the Composer Widget.
Plans are filtered and rated before being presented to end users.
When users select a plan for execution, the corresponding mashup is configured~(composition step two) by determining necessary widget instances and their wiring to enable \gls{IWC}.
The necessary information is provided with each plan.
The mashup platform is responsible for loading widgets requested by the Composer Widget.

Once all Application Widgets are loaded, users can execute the resulting mashup in order to make use of its functionality.
Because mashups may offer more functionality and interactions necessary to achieve the functionalities specified by corresponding planning goals, users may not be able to make out the proper mashup usage.
Therefore, execution needs to be instructed.
This kind of mashup usage is different from universal mashup composition~\cite{pietschmann:2011} which tries to find mashups without a specific use case.
In this case a specific use case, expressed with the goal functionalities, exists.
This means, resulting mashups are composed for a specific use case and are not designed to serve additional use cases not specified by goal functionalities.

The execution process is guided by a sequence of user instructions presented by the Composer Widget~(a feature not offered by the \gls{ACE}).
Each instruction states the action to be performed on some \gls{UI} element.
\Cref{chap:execution} outlines the execution process in detail.
Per \gls{VWM}, widgets communicate with one another via a message bus realizing the publish-subscribe pattern.
This message bus is part of the mashup platform.
The execution progress is monitored by examining the message bus for widget communication corresponding with the execution of specific widget actions.
Widget actions can modify their widget's internal state which gets published for each change.
The planner considers those communications and includes them in resulting plans.
When expected communication occurs~(depending on the current execution progress), the Composer Widget can assume certain actions to be executed successfully.
Corresponding instructions are marked as done and subsequent instructions can be addressed by the user.
