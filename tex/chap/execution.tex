\chapter{Assisted Mashup Execution}
\label{chap:execution}

Given a mashup created by an automated planner and selected by the end user, its actions must be executed in order to realize the mashup's intended functionality.
The loading of necessary widgets is trivial when the planner provides information about required widgets.
The biggest obstacle is to prevent widgets from interfering in a manner counter-productive to a mashup's intended usage as outlined in \cref{sec:mashup-composition}.
Partial-order plans~\cite[chap.~6]{ghallab:2004} offer such orderings while, at the same time, imposing no constraints on the execution order when not necessary~(\ie no interfering actions).
When such an ordering exists and is obeyed during execution, interferences can be avoided.
Any interference must be discovered and resolved during planning, by imposing a fixed execution order among interfering actions.
But not all interferences may be resolved when a planner considers only the minimal effects to realize some goal functionality.
Additional effects, not required for a mashup's functionality that may emerge during execution, as well as their effects on a mashup in its entirety, cannot be avoided from happening.
An action may publish additional, not required, messages or a widget may provide actions not used by the planner but still executable because widgets must be used in its entirety.
Such actions may be satisfied without requiring any dependencies not already considered during planning via other, required, actions.
This means that mashups must be executed under the assumption that interference, inevitably, will happen.



\section{Widget Isolation}

When the execution is in some step~\( t \), all actions in this step must be executed before execution can advance to \( t+1 \).
An action in \( t \) may publish a message interfering with execution of actions in later steps.
The cause for this lies in the publish-subscribe system used to realize \gls{IWC}.
Any widget can publish and receive messages independent of the current execution step.
Therefore, execution steps must be isolated to allow messages published by widgets in \( t \) to be received only by widgets with actions in \( t+1 \).

This isolation can be achieved by using a wrapper around each widget~\cite[sect.~3.3]{pietschmann:2011}.
Such a wrapper must provide the same \gls{API} the mashup platform provides to non-wrapped widgets because a widget's behaviour~(relying on some environment provided by the platform) must be maintained.
Wrappers, on behalf of their widgets, subscribe to all necessary messages and delegate them to their respective widgets.
Any published message gets delegated to the mashup environment.
In that sense wrappers present themselves as regular widgets to their environment.
The isolation is realized by configuring a wrapper in each execution step to delegate only message required by a mashup plan, namely those explicitly mentioned in a plan.
Another strategy would be to change the mashup platform's default publish-subscribe implementation to analyse each publication and filtering it depending on whether it is allowed in the current execution step or not.
This solution highly depends on the underlying implementation as it is necessary that each publication carries information about sender and receiver and that the filter has knowledge about the executed plan.
Both strategies require the configuration, of either the wrappers or the publication filter, to be handled by a central instance per mashup as part of the mashup platform.

This central execution instance must keep track of the current execution step in order to properly configure the environment.
An execution step is completed when all its actions have been performed.
To determine when an action has been executed can be realized by subscribing to \emph{all} messages an action will publish~(not only the minimal publications considered by the planner) and recording when all expected messages have been received.
However, some actions may not publish at all, in which case their execution cannot be determined.
In this case the system must be notified about a completed action by users themselves.
Both, wrapper and publication filter, allow to automatically determine a successful execution when possible.
When using wrappers, this can be realized completely internal~(without exposing any messages to the environment).
Wrappers must determine the successful execution and, when this is the case, notify the central execution instance about which action is done.
Using publication filtering, it is trivial to detect completed actions when all publications need to be analysed anyhow.



\section{Execution Instructions}

The mashup execution is not automatic, it is carried out by end users with likely no knowledge about the correct execution order.
Therefore, users must be instructed in executing a mashup.
Concretely, this means that users must perform all actions in the very first execution step, before being able to proceed to actions in subsequent steps.
Action within the same step can be executed in any order users may prefer.

User interactions are also required to solve a problem when using autonomous widgets in a mashup with fixed execution order.
When invoking an action in step~\( t \), its publications may trigger the automatic execution of actions in \( t+1 \)~(when no interactions were required), which in turn could likewise trigger actions in \( t+2 \).
When there are remaining actions in \( t \) to perform, the mashup would ``rush ahead'' of its proper execution~(performing actions in steps not due for execution).
User interactions serve as additional requirements for executing actions, \ie the actual invocation.

Some interference~(despite isolating widgets) cannot be prevented when users deviate from planned execution.
By invoking actions not part of a plan but still offered by a widget, users can cause effects that may cause interference.
This applies only to allowed effects~(others are prevented through isolation) which, however, can also be caused by any other action of the same widget.
Because these actions were not considered during planning, there are no precautions to prevent their interference.

Each action requires some instructions on how to be invoked once enabled.
A simple solution would be a verbal instruction~(\eg ``Provide your credit card information and click button `submit'.'').
This may, however, present a language barrier to users which can be mitigated to some extent by providing multiple versions in common languages.
For inexperienced users another problem arises because it may be difficult to link an instruction with the right \gls{UI} element~(\eg an input field or button).
Because such instructions are non-standard~(\ie plain text), their structure is subject to widget authors.

By relying on \gls{HTML} to create widget \glspl{UI}, it is possible to define instructions in a more standard and, most importantly, machine-readable manner.
Analysing the above-mentioned instruction, one can reason about the structure of instructions in general.
They have an action~(``provide''), an object~(``your credit card information''), and preferably a target~(not present in the aforementioned example, showing the issue with non-standard text).
The example instruction actually consists of two instructions, the second being the submission of credit card information.
Although the second instruction mentions the \gls{UI} element, users have to make a connection from text to actual \gls{UI} elements.
The trichotomy of action, object, and target allows the construction of instructions using a standard vocabulary.
\gls{DOM} Level 2 standardizes various event types~\cite[sect.~1.6]{w3c:dom2-events} which can be used as a basis for actions.
Such events include mouse events~(\eg clicks or movements on elements) or element state changes~(\eg change or selection of values).
Objects can be given by a semantic concept.
Instruction targets can either be uniquely identified by their \gls{DOM} element \glspl{ID} or selected with \gls{CSS}~Selectors~\cite{w3c:css3-selectors}.
The latter are preferable as they offer a concise syntax when selecting elements via their \glspl{ID}~\cite[sect.~6.5]{w3c:css3-selectors}.
With this information it is possible to generate textual instructions in a user's preferred language and establish connections to the necessary \gls{UI} elements, \eg by highlighting them using prominent colours.
Elements are selected by applying \gls{CSS} Selectors on a particular widget's \gls{DOM}.



\summary

Mashup execution must be assisted when mashups are composed based on plans created for some intent, \ie a set of functionalities.
Because mashups may allow more interactions than required by their plans, counter-measures must be taken to avoid interference between actions.
Some interferences may still occur, although interferences between actions used in a planning graph are ruled out.
This is not the case of actions not considered during planning when actions are not applicable to the planning problem.
These actions are still provided by widgets when used for a mashup as widgets must be used in their entirety.
Users may be able to perform interactions invoking these actions.
To overcome these issues, widgets need to be isolated.
This is either accomplished by wrapping every widget to allow only expected publications to be published and received.
Another solution is to filter publications from within the messaging system.
Wrappers and messaging-system-based filter need to be configured during execution to handle allow all expected publications depending on the execution progress.

Based on a mashup's plan which includes all necessary actions and, thus, necessary instructions, users can be assisted during execution.
The assistance is realized in form of a step-by-step list of instructions.
These instructions can be plain text, although a machine-readable description is preferable.
Plain text is disadvantageous because of non-standard language and because users may not be able to identify the right \gls{UI} elements as instruction targets.
A machine-readable format can be used generate instructions in a user's preferred language and to automatically link the right \gls{UI} elements.
The completion of instructions is determined based on their respective actions' expected publications.
In case no publications exist, \ie precursor actions or final actions, users must manually notify the execution process about an action's completion.
