\section{Mashup Planner Library}

The mashup planner is provided as a library written in Java.
This allows the planning algorithm to be used in other software solutions.
The reason for choosing Java is the use of \emph{Apache~Jena}%
  \footnote{\AccessedURL{2014}{9}{22}{http://jena.apache.org}}%
, a framework written in Java to process semantic data in \gls{RDF}.
Such frameworks also exist for other programming languages, \eg \emph{rdflib}%
  \footnote{\AccessedURL{2014}{9}{22}{https://github.com/RDFLib/rdflib}}
for Python.
However, \name{Pietschmann} et al. use Apache~Jena for processing semantic data for their universal mashup composition~\cite[sect.~4]{pietschmann:2011}.
So it basically comes down to using an established technology discovered during research for this thesis.

The library's \gls{API} is documented using \gls{UML} class diagrams.
Class diagrams tend to be large when including all information.
When a single diagram contains multiple classes to document their relations, the size further increases making it difficult to maintain a readable presentation when scaled down.
Therefore, class diagrams shown in this document omit certain common aspects.
Firstly, class attributes are read-only without exception, \ie their values cannot be modified via a class' public interface.
\gls{UML} defines the modifier \emph{readOnly} enclosed in curly braces behind each class attribute where applicable.
Secondly, all class attributes except derived attributes~(identified by a forward slash ``/'' prefixing the attribute name) are initialized via a class constructor.
Depending on the number of attributes, their names, and types, a constructor declaration can be quite long.
Furthermore, only the public interface is documented.
Public visibility is denoted by ``+'' in front of each public attribute and method.
By omitting these aspects it is possible to reduce a diagram's size.
\Cref{fig:uml-class-example} illustrates these simplifications compared to its canonical counterpart.

\begin{figure}
  \centering
  \includegraphics{fig/uml-class-example}
  \caption[Comparison of Simplified and Standard Class Diagram]{%
    Comparison of class diagrams in simplified~(left) and standard~(right) notation.
    Both diagrams document the same class with the simplified variant omitting redundant features.}
  \label{fig:uml-class-example}
\end{figure}



\subsection{Mashup Planner}

The mashup planner represents the central component and is responsible to find plans for a given goal mashup.
It implements the algorithms described in \cref{chap:planner}.
Class \emph{PlanningProcess}~(\cref{fig:uml-class-planning-process}) realizes the loop of alternating planning graph creation/extension and plan search.
A planning process is configured upon instantiation using dependency injection of
\begin{ienumerate}
  \item a \emph{MashupPlanner},
  \item a \emph{PlanCollector}, and
  \item a \emph{PlanningProblem}.
\end{ienumerate}
The underlying planning graph is publicly exposed.

\begin{figure}
  \centering
  \includegraphics{fig/uml-class-planning-process}
  \caption[Class Diagram of \emph{PlanningProcess}, \emph{PlanningProblem}, and Dependencies]{%
    Class diagram showing \emph{PlanningProcess}, \emph{PlanningProblem}, and dependencies.}
  \label{fig:uml-class-planning-process}
\end{figure}

A planning process is executed by invoking method \emph{advance}.
On each call a search for plans of a specific depth is performed after evolving the planning graph to this very depth.
A \emph{PlanningProblem} states the goal mashup and a range of possible plan depths.
The planning strategy is realized by implementing interface \emph{MashupPlanner}~(\cref{fig:uml-class-planner}) which realizes planning graph creation, extension, and plan search.
Before any plans are searched the planning graph is evolved to the minimal depth given by the planning problem to avoid searching for plans below the depth minimum.
The process stops when the maximal allowed plan depth will be exceeded.
\Cref{fig:uml-seq-planner} shows a \gls{UML} sequence diagram documenting a planning process' lifetime.

\begin{figure}
  \centering
  \includegraphics{fig/uml-seq-planner}
  \caption[Sequence Diagram for Mashup Planner]{%
    Sequence diagram describing the mashup planner's lifetime.
    Library users advance a planning process in a loop until done.
    When no planning graph exists, it is created, otherwise a new graph is created by extending the current graph.
    Extracted plans are provided to a plan collector for further processing.}
  \label{fig:uml-seq-planner}
\end{figure}



\subsubsection{Planning Strategy}

Interface \emph{MashupPlanner}~(\cref{fig:uml-class-planner}) combines the interfaces \emph{PlanningGraphFactory}, \emph{PlanningGraphExtender}, and \emph{PlanExtractor}.
These interfaces provide the planning graph creation, its extension, and the plan search respectively.
The library comes with a default implementation~(\emph{DefaultMashupPlanner}) relying on the strategy pattern to inject custom behaviour using the three mentioned interfaces.
These three interfaces have one default implementation each.
Classes \emph{DefaultGraphFactory} and \emph{DefaultGraphExtender} realize the planning graph construction in \cref{sec:planning-graph-construction}.
Class \emph{BackwardChainingPlanExtractor} realizes, as its name suggests, the backward-chaining plan search in \cref{sec:plan-search}.
By performing reachability analysis of the planning graph, it is possible to eliminate non-executable actions and thereby reducing search complexity.
The backward-chaining plan extractor searches plans one at a time by returning an iterator.%
  \footnote{\AccessedURL{2014}{9}{22}{http://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html}}
This provides plan consumers with the ability to request each plan individually~(in no particular order) allowing for search and processing to be interleaved.

\begin{figure}
  \centering
  \includegraphics{fig/uml-class-planner}
  \caption[Interface \emph{MashupPlanner} and Default Implementation]{%
    Interface \emph{MashupPlanner} and its default implementation \emph{DefaultMashupPlanner.}}
  \label{fig:uml-class-planner}
\end{figure}



\subsubsection{Plan Handling}

Found plans are handled via a dedicated \emph{PlanCollector}.
It allows plans to be handled individually by either processing them directly or storing them for later processing.
In its most trivial form the collector may be backed up by a list appending each plan at the end.
The immediate processing can be, for example, the rating of each plan, which is not an essential part of the planner itself.
In addition to handling resulting plans, plan collectors can also instruct a planning process on how to proceed after handling the current plan by returning an appropriate \emph{PlanCollectorResult}.
Value \emph{CONTINUE} states that a planning process should proceed normally.
By returning \emph{SKIP\_LEVEL} the search for plans in the current level should be stopped.
Subsequent calls to \emph{advance} continue with the next level, given a graph extension is possible.
Value \emph{STOP} causes a planning process to halt entirely, also ignoring any further plans.
Skipping levels or even stopping a planning process is useful when plans are found which are sufficient enough to satisfy user needs.
This avoids unnecessary search for further plans and additional graph extensions when it is unlikely that better plans will found.
Unnecessary search, when using \emph{SKIP\_LEVEL}, can only be avoided when search and processing are interleaved, \eg searching one plan at a time and processing it instead of extracting all plans in one go.



\subsubsection{Planning Graph Structure}

The planning graph structure implemented with class \emph{PlanningGraph}~(\cref{fig:uml-class-graph}) differs from the structure outlined in \cref{sec:planning-graph-structure}.
\emph{PlanningGraph} makes the instance relation between actions explicit by creating \emph{action provisions} for each unsatisfied required action.
An action provision contains the requested action, an optional precursor action, and zero or more property provisions based on published properties.
Depending on the precursor action, property provisions are used to satisfy properties requiring a present value not already provided by the precursor action.
When an action provision has no precursor action, it must contain at least one property provision.
An action may have more than one action provision, each providing a distinct combination of precursor actions and publishing actions.
The instance information is explicit by assigning a particular precursor action to a requested action per action provision.
This contrasts with the theoretical graph structure which passes the determination of instance information represented by action provisions to the plan search~(where instance information is required).
Making the information explicit reduces search complexity by increasing space complexity~(multiple action provisions per required action).
The resulting graph structure replaces each pair of requirement and action levels with provision levels.
An initial level uses functionality provisions.
Action provisions are used by extension levels.
This modification is possible because provisions provide information about actions required in the next level.
Information about compatible functionalities, properties, and propositions is made implicit.

\begin{figure}
  \centering
  \includegraphics{fig/uml-class-graph}
  \caption[Class Diagram of \emph{Plan}, \emph{PlanningGraph}, and Dependencies]{%
    Class diagram of \emph{Plan}, \emph{PlanningGraph}, and dependencies.}
  \label{fig:uml-class-graph}
\end{figure}



\subsection{Widget Repository}
\label{sec:semantic-widget-repository}

Access to widget knowledge is abstracted by interface \emph{Repository}~(\cref{fig:uml-class-repository}) responsible for finding actions for some request~(all widget actions, matching properties, or matching functionalities) and measuring the distance between types and functionalities.
The abstraction also allows using different systems for representing widget information.
The default implementation relies on semantic-based widget representations and uses Apache~Jena.
It is plausible to represent the knowledge this way, as semantic concepts are used to describe types and functionalities.
This also offers two advantages over representing information in a custom format, \eg \gls{XML} based on some \gls{XML}~Schema.
Firstly, machines can infer knowledge~(the major reason behind semantic data) from stated information along with knowledge about referenced ontologies.
Secondly, definitions of additional concepts must not modify the original format definition, new ontologies can be created and referenced in any semantic dataset.

\begin{figure}
  \centering
  \includegraphics{fig/uml-class-repository}
  \caption{Interface \emph{Repository}}
  \label{fig:uml-class-repository}
\end{figure}



\subsubsection{Mashup Ontology}

Each widget to be covered by the planner must provide a description of its interface.
This requires an ontology describing the necessary semantic concepts.
This \emph{Mashup Ontology} is defined with \gls{OWL}.
\Cref{fig:mashup-ontology} illustrates the mashup ontology graph.
For a formal definition refer to \cref{lst:mashup-ontology}.
The ontology provides concepts to describe widgets and goals for the planner.
The widget-related concepts follow the modelling presented in \cref{sec:planning-graph-structure} and are self-explaining.
Planning goals are described by instances of concept~\emph{Mashup} referencing one or more \emph{Functionality} concepts to be realized by conforming mashups.

\begin{figure}
  \centering
  \includegraphics{fig/ontology}
  \caption[Mashup Ontology Graph]{%
    The Mashup Ontology graph illustrating the relations between concepts.
    Colours are used to easily identify concept types~(see legend).
  In order to maintain a readable graph, cardinality constraints are omitted~(properties \emph{:hasName}, \emph{:hasInstructionText}, \emph{:hasEffects}, and \emph{:hasPreConditions} have \emph{owl:cardinality} 1, \ie a subject cannot have more than one of each predicate).}
  \label{fig:mashup-ontology}
\end{figure}



\subsubsection{Semantic-Based Implementation}

The planner library comes with a semantic-based default repository implementation.
Method \emph{getWidgetActions} finds all actions of a particular widget.
This can be realized by a \gls{SPARQL} query~(\cref{lst:widget-actions}) selecting all actions connected to the widget in question via property \emph{:hasAction}.
This query, however, is trivial because it contains a simple triple as its only statement.
Such queries can be easily realized with Apache~Jena's graph \gls{API} which allows finding all triples based on some fixed subject and property in this case.

\begingroup
  \captionof{listing}[\acrshort{SPARQL} Query Selecting Widget Actions]{%
    Parametrized \gls{SPARQL} query asking for all actions of some widget.
    Results will be provided via variable \emph{action}.
    Variable \emph{widget} gets substituted for a particular widget's identifier before executing the query.
    \label{lst:widget-actions}}
  \sparqlfile{lst/sparql/widget-actions.rq}
\endgroup

Finding actions realizing/publishing compatible functionalities/properties~(method \emph{findCompatibleOffers}) can be easily accomplished by considering inheritance relations between requested and offered concepts.
Inheritance of functionalities is realized with property \emph{:subFunctionalityOf}.
Properties are matched based on their types defined by classes in \gls{RDFS} with subtypes denoted by property \emph{rdfs:subClassOf}.
The search for compatible functionalities and properties is not as trivial as finding widget actions.
Because the depth of the functionality/type hierarchy is not known, it would require a traversal of the \gls{RDF} graph in order to find all concepts related to a particular request via an arbitrary long inheritance chain.
\gls{SPARQL}~1.1~(supported by Apache~Jena) provides the concept of property paths allowing to concisely state the paths to match.
In contrast to a single property~(see \cref{lst:widget-actions}) between subject and object, property paths allow complex relations between subjects and objects to be matched.
Property paths are structured similar to regular expressions by using grouping, repetition, alternation, and negation to construct path expressions from properties acting as atomic building blocks.
The queries are given in \cref{lst:compatible-functionalities} and \cref{lst:compatible-properties} respectively.

\begingroup
  \captionof{listing}[\acrshort{SPARQL} Query to Select Actions Realizing Compatible Functionalities]{%
    Parametrized \gls{SPARQL} query selecting actions realizing a compatible functionality.
    Considers paths of any length along property \emph{:subFunctionalityOf}.
    Matching actions will be available via variable \emph{action} along with their matching provided functionality in variable \emph{offer}.
    Variable \emph{request} gets substituted for the requested functionality's identifier before execution.
    \label{lst:compatible-functionalities}}
  \sparqlfile{lst/sparql/compatible-functionalities.rq}
\endgroup

\begingroup
  \captionof{listing}[\acrshort{SPARQL} Query to Select Actions Publishing Compatible Properties]{%
    Parametrized \gls{SPARQL} query selecting actions publishing a compatible property.
    Considers paths of any length along property \emph{rdfs:subClassOf} starting at an offered property's type.
    Variables \emph{action} and \emph{offer} make each action and their respective matching property available.
    Variable~\emph{request} gets substituted for the requested property's identifier before execution.
    \label{lst:compatible-properties}}
  \sparqlfile{lst/sparql/compatible-properties.rq}
\endgroup

Unfortunately, property paths in \gls{SPARQL}~1.1 do not allow measuring the length of matched paths, an issue also addressed in the \gls{W3C} working draft~\cite[sect.~2]{w3c:sparql11-property-paths}.
This feature would make it simple to determine the distance between compatible concepts~(repository method \emph{getDistance}) necessary for rating actions, widget, and mashups.
Nevertheless, it is possible to find the distance using \name{Dijkstra}'s algorithm~\cite{dijkstra:1959} for finding the shortest path between two nodes in a graph.
With the use of Apache~Jena's graph \gls{API} it is trivial to traverse a graph as required by the algorithm.
Starting from an origin node, identifying the requested functionality or type, its neighbours connected by directed edges labelled with property \emph{:subFunctionalityOf} or \emph{rdfs:subClassOf}, depending on which distance is measured, are considered recursively.
This process is repeated until the destination node, identifying the offered concept, is found.
