\section{Mashup Composer Widget}

The Mashup Composer Widget is a \gls{W3C}~Widget~\cite{w3c:widgets} providing users with the ability to consume the mashup planning service and execute resulting mashups.
Its \gls{UI} is realized with \emph{Backbone.js}%
  \footnote{\AccessedURL{2014}{9}{22}{http://backbonejs.org}}
which offers a framework for developing web applications following the \gls{MVC} pattern.
The widget provides users with a form~(\cref{fig:widget-ui-request}) to specify goal mashup, planning constraints, and action composition strategy~(\cf\cref{sec:service-request}).
Plans received from the planning service are presented in a list~(\cref{fig:widget-ui-plans}) sorted by rating.
A plan's relative rating among all plans is visualized using a filled bar.
From this list, users can select plans to load and execute.
The widget creates an execution plan using information returned by the planning service~(\cf\cref{sec:service-response}).
The execution plan is presented in a third dialogue~(\cref{fig:widget-ui-execution}) which lists all steps and instructions that need to be performed.

\begin{figure}
  \centering
  \begingroup
  \setlength{\fboxsep}{0pt}%
  \setlength{\fboxrule}{.5pt}%
  \fcolorbox{black!8}{white}{\includegraphics[width=.998\textwidth]{img/ui-widget-request-1}}
  \endgroup
  \caption[Request Form of Mashup Composer Widget]{%
    Request form of Mashup Composer Widget.
    The goal mashup~(written in \gls{Turtle}) requires functionality \emph{SearchAccommodation}.
    Users need to select a language when it cannot be automatically determined from the goal mashup description text.
    The plan depth is constrained to range \( [1..4] \).
    The action composition strategy is specified to compose minimal precursor actions as well as functionality- and property-providing actions.
    The planning service is provided via \emph{http://localhost:9000/planner}.}
  \label{fig:widget-ui-request}
\end{figure}

\begin{figure}
  \centering
  \begingroup
  \setlength{\fboxsep}{0pt}%
  \setlength{\fboxrule}{.5pt}%
  \fcolorbox{black!8}{white}{\includegraphics[width=.998\textwidth]{img/ui-widget-plans}}
  \endgroup
  \caption[Plan Selection in Mashup Composer Widget]{%
    Plan selection view of Mashup Composer Widget.
    Plans are selected via the blue buttons showing a triangle shape.
    The relative rating among all plans is given visually by a filled bar as well as textual in percent.
    The numbers of involved widgets~(w), required steps~(s), and interactions~(i) to perform are provided as well.}
  \label{fig:widget-ui-plans}
\end{figure}

\begin{figure}
  \centering
  \begingroup
  \setlength{\fboxsep}{0pt}%
  \setlength{\fboxrule}{.5pt}%
  \fcolorbox{black!8}{white}{\includegraphics[width=.998\textwidth]{img/ui-widget-execution}}
  \endgroup
  \caption[Plan Execution in Mashup Composer Widget]{%
    Plan execution view of Mashup Composer Widget.
    The execution requires three steps.
    The first step is already done, visualized by faint text colour and struck out background.
    Step~2 is visually highlighted as the current step.
    Remaining steps, step~3 in this case, is rendered similar to completed steps except no special background.
    Each step can be collapsed in which case only its number~(top right corner) is visible.
    Widgets are highlighted by clicking buttons with flash icons~(on the left side).
    The group of four buttons on top allow expanding/collapsing all execution steps~(two leftmost buttons) and reloading/clearing the mashup~(two rightmost buttons).}
  \label{fig:widget-ui-execution}
\end{figure}

Plans are rated to measure their corresponding mashups' complexities.
The number of required interactions provide a simple means to estimate a mashup execution's approximate complexity.
This, however, assumes all interactions having equal costs for users to perform.
Apparently this is not the case when considering the issues of \gls{HCI} where some interactions may provide much higher barriers for some users than to other users.
Some interactions may even be entirely impossible to fulfil for certain users.
The final rating is the sum of the number of interactions, the number of widgets, the number of execution steps~(plan depth), and the distance between semantic concepts given for each request/offer pair.

Some plans may not be executable at all.
The planning algorithm only considers the data type of properties when selecting compatible actions.
Using the \gls{VWM}, data flow between widgets is only possible for properties of the same name determining the publish-subscribe channel data is published to/received from.
Assuming it is possible to resolve this form of incompatibility, the planning algorithm ignores property names.
The execution model used by the composer widget, however, does not resolve these incompatibilities, therefore making it necessary to filter out non-executable plans.

\Cref{fig:uml-seq-client-1} show a sequence diagram depicting the composer widget's lifetime up to plan presentation.
Mashup creation and execution is documented by a second sequence diagram in \cref{fig:uml-seq-client-2}.
The diagram is split into two parts because it would be too large to be shown in a single diagram.

\begin{figure}
  \centering
  \includegraphics{fig/uml-seq-client-1}
  \caption[Sequence Diagram for Mashup Composer Widget~(1)]{%
    Sequence diagram describing the Mashup Composer Widget's lifetime up to plan presentation.
    Users create an empty mashup and add the Composer Widget.
    After specifying the goal mashup and submitting the planner request, the widget consumes the planning service.
    Resulting plans are filtered, rated and presented to the user.
    In case no plans are found or planning failed, a failure message is presented.}
  \label{fig:uml-seq-client-1}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{fig/uml-seq-client-2}
  \caption[Sequence Diagram for Mashup Composer Widget~(2)]{%
    Sequence diagram describing the Washup Composer Widget's lifetime after plan selection.
    The diagram continues \cref{fig:uml-seq-client-1} with identically arranged objects.
    Selecting a plan causes the current mashup to be cleared and populated with new widgets determined during execution plan creation.
    Execution steps are presented to the user who processes any remaining actions until execution is complete.
    Execution process is updated after each action was executed.
    An action's completion is determined either by monitoring its publications or by requiring users to notify the Composer Widget about completed actions.
    The latter case is necessary when an action has no publications.}
  \label{fig:uml-seq-client-2}
\end{figure}



\subsection{Mashup Creation}

Although it is desirable for widgets to not rely on specific execution environments to achieve reusability, the Composer Widget relies on \emph{Apache~Rave}%
  \footnote{\AccessedURL{2014}{9}{22}{http://rave.apache.org}}
to configure a mashup according to some selected plan.
Apache~Rave provides a JavaScript-\gls{API} to search widgets and add them to an execution environment.
Based on the instance information given in a planning graph~(precursor relations) the Composer Widget determines each action's widget instance, allowing multiple instances of the same widget.
An existing mashup is cleared before loading a new mashup.
Apache~Rave offers a collection of all widgets in the current environment.
Only the Composer Widget, which is part of the very same environment hosting its mashups, must be filtered out to avoid its removal.
When adding a new widget, Apache~Rave performs an asynchronous request to its server.
This allows adding multiple widgets concurrently.
However, it turns out that Apache~Rave's server cannot handle that many requests concurrently or even in quick succession.
To avoid this problem, widgets are added sequentially with a short delay in between, \ie each widget is added once the previous request successfully completed.
A delay of one second was large enough to avoid problems whereas delays of half a second still caused occasional loading errors during testing.
After the last widget was added the mashup is ready for execution.



\subsection{Mashup Execution}

Plans are executed as outlined in \cref{chap:execution}.
Graph levels are processed sequentially converse to extension order.
For a level to be completed all of its actions must be performed successfully, \ie they must achieve their postconditions.
The instructions associated with every action are used to present users with a list of open tasks to be performed on the mashup~(\cf\cref{fig:widget-ui-execution}).
Tasks in the same execution step are grouped by the widget in which they must be performed.
Because it may not be apparent which widget a task targets, the \gls{UI} offers to highlight the respective widget by clicking a button next to the group of instructions of each widget.

The execution model does not allow actions to be repeated, \eg in case users want to correct an input made during an already performed action.
It would be possible to reset the execution state to a desired step containing the action to be repeated.
However, the internal states of participating widgets cannot be reset to states they had during specific execution steps.
This would require undoing any changes made after the execution step in question.
The only possibility is to reset the entire execution and reloading the entire mashup, requiring users to start again at the very beginning.
This functionality is provided via a button as part of the execution \gls{UI} and is effectively the same as selecting the plan without the need to consult the list of available plans.



\subsubsection{Widget Isolation}

Widgets are isolated by modifying Apache~Rave's publish-subscribe infrastructure, in contrast to the second variant of wrapping each widget.
Apache~Rave realizes its publish-subscribe system using \emph{OpenAjax Hub 2.0}~\cite{open-ajax-hub}.
Widgets requiring this feature~(expressed in their configuration) are provided with a so-called \emph{hub} providing, among others, the methods \emph{subscribe} and \emph{publish} offering, as their name suggests, the publish-subscribe functionality.
Hubs do not allow modifying the underlying system's behaviour.
This functionality is provided by \emph{managed hubs}~(a specialisation of general hubs) which allow managing the system's behaviour.
The managed hub is part of the mashup execution environment and not exposed to each widget to prevent them from modifying the system's behaviour.
JavaScript, nevertheless, allows code executed in an iframe~(which is the case for widgets) to access its parent window's context~(hosting the mashup execution environment).
This, however, is only possible when iframe and parent window contents are on the same domain as required by the same-origin policy.%
  \footnote{\AccessedURL{2014}{9}{22}{http://www.w3.org/Security/wiki/Same_Origin_Policy}}
This is the case for Apache~Rave where widgets are served under the platform's domain.

A managed hub, upon construction, is provided with a callback function invoked on each publication and responsible for determining if a publication is allowed, in which case it gets broadcast to its subscribers.
Because of JavaScript's dynamic nature it is possible to modify an existing managing hub to inject logic necessary to detect/filter publications and keep track of execution progress using the mentioned callback function.
Managed hubs do not provide a public interface to change this callback.
Instead, the property holding the callback, an implementation detail, must be assigned a new callback.
This makes the execution process implementation brittle to changes of the managed hub's implementation.

The callback is invoked with objects describing the publisher, subscriber, topic, and payload.
Based on the given publisher and subscriber, it is possible to determine the corresponding widgets.
Depending on the current execution step and knowledge about necessary publications~(provided by the plan), the publication can either be allowed or forbidden.
Although the publication is not actually done at this point, it can be assumed it will have its expected effects.
With this knowledge, actions can be marked as executed when all of its expected publications were detected.
Actions without any publications require users to notify the execution process about their completion.
Publications not required in the current execution step are prevented, thereby realizing widget isolation.
