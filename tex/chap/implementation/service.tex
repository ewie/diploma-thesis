\section{Mashup Planning Service}

The mashup planner is exposed via a web service separate from end user's mashup platforms.
The service uses \emph{Java Servlets} to provide a thin layer atop the actual mashup planner.
Servlets provide the logic to respond to requests received by a web server executing the servlets.
The prototype uses \emph{Jetty}\footnote{\AccessedURL{2014}{9}{22}{http://www.eclipse.org/jetty/}} to realize a \gls{HTTP} server.
The server exposes the endpoint \texttt{POST~/planner} to consume the mashup planner.
Using the POST method, it is possible to include a request body containing data required by the planner.
The same data could also be transmitted using a GET request by embedding the data in a request \gls{URL} instead of its body.
However, the amount of data is constrained by the maximum \gls{URL} length imposed by a client's technology, whereas \gls{HTTP} version 1.1 places no limit on \gls{URL} lengths~\cite[sect.~3.2.1]{rfc:2616}.
Different web browsers support varying \gls{URL} lengths.
Microsoft's \emph{Internet Explorer}, for example, limits \glspl{URL} to 2083 characters.%
  \footnote{\AccessedURL{2014}{9}{22}{http://support.microsoft.com/kb/208427}}
The service uses \gls{JSON} to serialize all incoming and outgoing data to simplify its processing in JavaScript which natively handles the parsing and generating of \gls{JSON}.
Java offers a standard \gls{API} for processing \gls{JSON}.%
  \footnote{\AccessedURL{2014}{9}{22}{http://jsonp.java.net/}}
\Cref{fig:uml-seq-service} depicts the service's lifetime using a \gls{UML} sequence diagram.

\begin{figure}
  \centering
  \includegraphics{fig/uml-seq-service}
  \caption[Sequence Diagram for Mashup Planning Service]{%
    Sequence diagram describing the mashup planning service's lifetime.
    Before any requests are served, the service optionally initializes its database with widgets found on the file system.
    When an existing, already populated, database is used this initialization can be skipped.
    Requests are processed until the service gets shut down.
    Each request is handled via a planner job~(configured with a request's parameters) interacting with the planner library.}
  \label{fig:uml-seq-service}
\end{figure}

The planning service uses Apache~Jena's \gls{RDF} storage \emph{TDB}%
  \footnote{\AccessedURL{2014}{9}{22}{http://jena.apache.org/documentation/tdb/}}
to store semantic data describing each widget.
The database can be in-memory~(implies an empty database on each start) or file-system-based depending on the web service's configuration.
On startup, the database can optionally be populated.
This may not be necessary when an existing database, with existing data, is used.
Widgets are loaded from the file system after the web server has been started, before serving any requests.
The service recursively traverses a directory, given as configuration parameter, to collect files.
These files are passed to Apache~Jena reading each file and trying to parse its content after guessing its data format.
Apache~Jena determines the data format based on file extensions found in \glspl{URL}.
Files of unknown format are ignored.
The data, successfully read from each file, is finally added to the database's triple store.
Seeding a database multiple times with the same data presents no problems as triples are either already present or new.
Adding the same triple more than once has no effects.



\subsection{Request}
\label{sec:service-request}

A planning request contains three properties:
\begin{ienumerate}
  \item the mashup description,
  \item the allowed plan depths, and
  \item the action composition strategy.
\end{ienumerate}
The request structure is defined with \gls{JSON}~Schema%
  \footnote{\AccessedURL{2014}{9}{22}{http://json-schema.org}}
given in \cref{lst:json-request}.
The mashup description represents the planning goal in form of a semantic description using the Mashup Ontology~(\cref{fig:mashup-ontology}, \cref{lst:mashup-ontology}).
The semantic data is either given as text along with a media type denoting its language or as \gls{RDF}/\gls{JSON} embedded directly in the request data.
Supported formats for the textual variant are
\begin{itemize}
  \item \gls{RDF}/\gls{JSON}~\cite{w3c:rdf-json}~(application/rdf+json),
  \item \gls{RDF}/\gls{XML}~\cite{w3c:rdf-xml}~(application/rdf+xml), and
  \item \gls{Turtle}~\cite{w3c:turtle}~(text/turtle).
\end{itemize}
Because the request itself is serialized using \gls{JSON} it makes sense to embed the semantic data directly using \gls{RDF}/\gls{JSON}~(instead of using a character string with the serialized \gls{RDF}/\gls{JSON}).
It can be argued if the sole reliance on embedded \gls{RDF}/\gls{JSON} would be sufficient.
However, a textual representation such as \gls{Turtle} is better suited for human processing as it is designed to be a compact representation for \gls{RDF}.
Such a representation can, of course, be parsed and converted into \gls{RDF}/\gls{JSON}.
Demanding clients to perform this task, when it can also be performed by the service, imposes an unnecessary requirement.

The depth of resulting plans can be constrained to ensure the planner will not return arbitrarily large plans.
When the request does not constraint the plan depth, the range~\( \left[1..2^{31}-1\right] \)~(the positive half of signed 32-bit integers) is used, effectively allowing plans of arbitrary depth.
This assumes plans not exceeding \( 2^{31}-1 \) execution steps, resulting in unreasonable large mashups anyway.

Action composition can affect the planning quality~(search space and number of plans).
Depending on the strategy, it is even possible that no plans are found.
To make the strategy configurable, it can be specified with each request.
The strategy addresses the composition of actions providing functionalities or properties and the creation of precursor actions.
Providing actions are either composed or used as discovered through the repository.
Precursor actions allow four strategies:
\begin{ienumerate}
  \item no composition at all~(\ie atomic actions),
  \item minimal precursors,
  \item extended precursors based on atomic precursors, and
  \item extended precursors based on minimal precursors.
\end{ienumerate}
Precursor actions are minimal when they achieve the minimal preconditions of another action only achievable within the same widget instance.
Extended precursors on the other hand achieve additional preconditions not requiring a precursor, \ie they could also be provided by a separate widget instance.
\Cref{lst:ex:request} shows an example request using embedded \gls{RDF}/\gls{JSON}.

\begingroup
  \captionof{listing}[Example Planning Request]{%
    An example planning request with depth range~\( [1..4] \).
    The planner must find mashups realizing functionalities \emph{urn:example:fn:SearchHotel} and \emph{urn:example:fn:BookHotel}.
    Actions providing functionalities and properties may be composed.
    The planner should also use action composition to form minimal precursor actions.
    The request's mashup description is also given in Turtle as informative reference.
    Using the Turtle variant in the request example would break readability as \gls{JSON} does not allow multi-line string literals.
    \label{lst:ex:request}}
  \jsonfile{lst/json/request-example.json}
  \turtlefile{lst/turtle/request-example-goal.ttl}
\endgroup



\subsection{Response}
\label{sec:service-response}

The planning response contains either an array of plans or, in case of failure, a message stating the reason for failure.
Each plan provides the underlying graph structure consisting of one initial level and zero or more extension levels.
Serialized plans do not contain all information encoded in the planning graph.
Information used only by the planning process but not necessary during execution is omitted.
This is the case for pre- and postconditions which are only required to figure out the ordering among actions.
This ordering, however, is made explicit by a planning graph's structure.
\Cref{lst:json-response} contains the response schema.
The plan schema is given in \cref{lst:json-plan}.
An example response is presented in \cref{lst:ex:response}.

\begingroup
  \captionof{listing}[Example Planning Response]{%
    Example planning response containing a single plan.
    The plan realizes the functionality \emph{SearchAccommodation}~(l.~4) using an action of widget \emph{HotelSearch}~(ll.~7--12) offering derived functionality \emph{SearchHotel}~(ll.~5 and 9--10).
    The action requires a geographical region and a date range to find hotels~(not explicitly mentioned in the serialized plan).
    Compatible types are offered by a map widget~(ll.~29--35) and calendar widget~(ll.~43--49) respectively.
    The appropriate actions are used for two property provisions~(ll.~22--35 and 36--49) in a single action provision~(ll.~14--49) for the hotel search action.
    \label{lst:ex:response}}
  \jsonfile{lst/json/response-example.json}
\endgroup

Actions in a serialized plan can occur multiple times when an action required in level~\( i \) is referenced as requested action in level~\( i+1 \)~(compare ll.~8--12 and 16--20 in \cref{lst:ex:response}).
When deserializing plans, reoccurring objects, although equal in value, will result in distinct object instances.
Determining if two objects denote, for example, the same action~(one object providing the required action in level~\( i \), the other object providing the requested action in level~\( i+1 \)) is not trivial as it requires a structural comparison of these object.
This can be solved by collecting, in this case, all actions and including them in the response using an array.
The actions can then be referenced by index where required.
When deserialized, identical actions can be detected by determining if two objects are the same instance.

This strategy however requires to be part of a structure's \gls{JSON}~Schema and must also be applied to concepts other than actions, \eg properties and functionalities.
\gls{JSON}~Pointers provide a generalization for this problem~\cite{rfc:6901}.
Using a path in form of a character string reaching into a \gls{JSON} document starting at its root, it is possible to reference any property's value~(although primarily used for objects and arrays).
Using the example in \cref{lst:ex:response} it is, for example, possible to rewrite ll.~15--20 to reference the action from the initial level.

\begingroup
  \jsonfile{lst/json/response-example-json-pointer-snippet.json}
\endgroup

Clients can request the use of \gls{JSON}~Pointers using the \gls{HTTP} \emph{Accept} header with value \emph{application/json; json-pointer=true}.
The parameter \emph{json-pointer} indicates the usage of \gls{JSON}~Pointers when assigned the value \emph{true}.
Value \emph{false}~(equivalent to omitting the parameter altogether) explicitly states content to be regular \gls{JSON}.
