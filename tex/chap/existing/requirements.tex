\section{Requirement Analysis}
\label{sec:requirements}

The problems and scenario presented in \cref{chap:introduction} allow the identification of requirements.
These will be divided into functional and non-functional requirements~(denoted with \protect\ReqIcon{F} and \protect\ReqIcon{N} respectively).
Functional requirements specify behaviour that needs to be realized, whereas non-functional requirements address quality issues beyond a system's required behaviour.
Each requirement has certain criteria to decide if a solution, \ie presented approaches as well as the resulting prototype implementation, satisfied a particular requirement.



\subsection{Functional Requirements}

\begin{ReqDefs}{F}

  \ReqDef{create-mashups}{Create executable mashups.}
    The result must be executable mashups, \ie users must be able to utilize a mashup's functionality.
  \ReqDef{realize-functionalities}{Mashups must realize all end-user-defined functionalities.}
    Resulting mashups must provide the functionality intended by users.

  \ReqDef{widget-model}{Mashups must support widgets conforming to the \acrlong{VWM}.}
    The \gls{VWM} must be supported in order to make use of existing widgets supporting this model, instead of requiring the development of new widgets conforming to the \gls{VWM}.

  \ReqDef{complete-mashups}{Mashups must be complete.}
    Resulting mashups must have no unsatisfied requirements.
    This avoids mashups which cannot realize the goal functionality when some requirements cannot be satisfied during execution.

  \ReqDef{detect-incompatibilities}{Mashups must use compatible widgets.}
    Widgets must be compatible in order to communicate and realize the data flow necessary for a mashup to achieve its intended functionality.

  \ReqDef{define-functionalities}{Let end users define required mashup functionalities.}
    Users must be able to express the functionality intended to be achieved with the execution of resulting mashups.

  \ReqDef{feedback}{Provide feedback when composition fails.}
    The feedback must state a descriptive reason why composition failed.

  \ReqDef{select-mashups}{Let end users select mashups to execute.}
    Users must be able to select particular mashups for execution from all resulting mashups.

  \ReqDef{load-mashups}{Load mashups into an execution environment.}
    User must be able to select a mashup and directly execute it without any further actions not essential to a mashup's actual functionality.

  \ReqDef{assist-users}{Assist users executing necessary steps to achieve a mashup's intended functionalities.}
    Mashup usage may not be self-explanatory, firstly due to the heterogeneous nature of widgets~(differing \gls{UI} designs) and secondly because the order of execution of individual actions may not be apparent to users.

  \ReqDef{execution-steps}{Determine execution steps necessary for realizing mashup functionalities.}
    Per \ReqRef{assist-users}, a composition engine must determine a valid execution strategy which allows users to achieve a mashup's functionalities.

  \ReqDef{prevent-interference}{Prevent widgets from interfering with a mashup's intended functionalities.}
    Widgets must not interfere with one another when not required by a mashup's functionalities.

  \ReqDef{prevent-feedback-loops}{Prevent feedback loops.}
    Feedback loops must not occur during mashup execution as they would introduce unwanted behaviour.

\end{ReqDefs}



\subsection{Non-Functional Requirements}

\begin{ReqDefs}{N}

  \ReqDef{minimal-widgets}{Select only widgets satisfying necessary requirements.}
    To limit mashups to a bare minimum, they should be solely composed of widgets necessary in realizing the goal functionality.

  \ReqDef{efficient-composition}{Efficiently compose mashups.}
    Mashup composition should be time- and space-efficient.
    There exists no definition what constitutes an efficient algorithm.
    However, algorithms of polynomial time are generally considered efficient as a rule of thumb~\cite{cobham:1964}.
    Algorithms of polynomial time have running time with an upper bound defined by a polynomial of their input size~\( n \)~(\( \BigO(n^k),k\geq1 \))~\cite[sect.~7.2]{sipser:2013}.
    However, depending on the input size \( n \), algorithms of exponential time~(\( \mathcal{O}(k^n),k\geq1 \)) can be more efficient.
    This aspect is not immediately apparent, as coefficients and lower-order terms, describing problem and algorithm characteristics, are ignored when analysing complexity~\cite[p.~276]{sipser:2013}.
    For example, polynomial \( f(n)=(10n)^{10} \) grows faster than exponential \( g(n)=10^{n/10} \) for \( n\leq355,n\in\mathbb{N} \).\footnotemark{}
    For \( n>355 \), \( f(n) \) grows less than \( g(n) \).
    This means, for input sizes \( 0..355 \) an algorithm whose running time is described by \( g(n) \) will be more time efficient than an algorithm with running time \( f(n) \).
    Brute-force search typically results in exponential time algorithms~\cite[p.~285]{sipser:2013}.
    \footnotetext{Wolfram|Alpha is a wonderful tool to quickly solve equations, like a set of simultaneous equations. Solution for \( n \) in \( (10n)^{10}=10^{n/10} \): \AccessedURL{2014}{9}{22}{http://www.wolframalpha.com/input/?i=solve+\%2810n\%29^10\%3D10^\%28n\%2F10\%29+over+the+reals}.}

  \ReqDef{ranking-by-complexity}{Mashups should be ranked by their complexity.}
    When multiple mashups can satisfy user needs, some mashup has to be chosen for execution.
    Because users cannot judge mashups prior to execution, the composer should rank mashups based on their complexity, \ie number of widgets or number of necessary execution steps.
    This ranking information should be provided to users.
    This is an extension to requirement \ref{req:select-mashups}.

  \ReqDef{ranking-by-preferences}{Mashups should be ranked by their suitability in fulfilling end user needs.}
    Ranking mashups based only on their complexity~(\cf requirement \ReqRef{ranking-by-complexity}) does not account for the suitability of mashups for particular end user groups.
    Therefore, end user preferences should be taken into consideration when rating mashups.

\end{ReqDefs}
