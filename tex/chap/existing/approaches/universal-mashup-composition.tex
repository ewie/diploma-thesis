\subsubsection{Universal Mashup Composition}
\label{sec:universal-mashup-composition}

\name{Pietschmann} et al. propose the concept of \emph{universal mashup composition}~\cite{pietschmann:2011}.
By using a \gls{UCM} describing widgets, web services, and data sources~(\eg feeds and databases) they are able to incorporate them into a single mashup.
Components are modelled in terms of three concepts:
\begin{itemize}
  \item properties modelling their internal states,
  \item operations exposed to the outside, and
  \item events to notify their environment.
\end{itemize}
Each property and operation/event parameter has a data type based on semantic concepts.
When concepts are shared between components~(\eg by using standardized ontologies), semantic incompatibilities can be detected and thereby avoided.
To cope with syntactical differences between components on the data level, each component must define a transformation from its data representation to a common representation.
Components may use \gls{JSON} or \gls{XML} with a custom schema to serialize any data sent via events or received via operations.
The data types defined in \gls{XSD} serve as a common representation as it provides the basic data types used by \gls{RDF} and \gls{OWL}~\cite{w3c:xsd-datatypes-rdf-owl}.

The paper does not present an algorithm for the automated planning of mashups.
Instead, it relies on an experienced mashup author who defines a mashup by using concrete components as well as templates.
Template components specify an interface~(\ie properties, operations, and events) required by other components or to realize the mashup's functionality in general.
Templates are resolved with concrete components using automated discovery and selection of matching components, \ie components which satisfy the interface.

Discovery and selection of components is based on compatibility of semantic data types~(functional requirements) and context information~(non-functional requirements).
The more exact a candidate type~(offered by a component registry) matches a requested type~(given by a template component) the more a component is suited to satisfy a template.
The distance along the inheritance chain between two types serves as the measure on how close two types are related.
When matching operations, a candidate must provide types which are equal or more general~(\ie supertypes of the requested type) so that the candidate can cope with any data it gets presented.
The opposite applies when matching properties and events.
In this case a candidate must provide types which are equal or more specific~(\ie subtypes of the requested type).
This way it is guaranteed that a parameter or event provides only types within the bounds defined by the template's interface.

Discovery and selection does not remove syntactical differences.
To resolve these during runtime, a mediation process between components must be applied.
The authors list four tasks that may be necessary:
\begin{enumerate}
  \item renaming properties, operations, events, and parameters,
  \item rearranging and/or filtering parameters,
  \item performing the aforementioned transformation, and
  \item lifting and lowering of semantic data types.
\end{enumerate}
Tasks 1--3 are carried out by a wrapper configured by the runtime environment and specific to each component.
This way the actual component remains untouched.
Task 4 will be realized by a mediator as part of the runtime environment.



\paragraph{Evaluation}

The \gls{UCM} is a generalization of the \gls{VWM} described in \cref{sec:widget-model} because of the trichotomy of properties, operations, and events.
The concept of properties is identical in both modellings.
Operations correspond to actions used by the \gls{VWM} in the sense that they describe means of invoking certain behaviours in a component.
The difference is that operations accept parameters to inject data whereas actions offer no parameters at all because data is injected separately via property-specific messages.
The same applies to events which are used to communicate states changes to other widgets in both modellings.
Events from the \gls{UCM}, however, are not necessarily bound to properties.
Instead, any operation can trigger arbitrary events which therefore also support multiple parameters.
Nevertheless, the \gls{UCM} can be used to describe widgets conforming to \gls{VWM}.

The composition process is not end-user-oriented.
Instead, it relies on skilled users with knowledge about the domain for which a mashup is required.
By providing assistance in finding matching components, mashup authors are relieved from manual search for suitable components.
This, however, is only possible when authors know the required interfaces necessary to describe a template.
This means authors still need sufficient insight into component design in order to extract information about possible interactions between components.
