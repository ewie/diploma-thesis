\subsubsection{Graphplan}
\label{sec:graphplan}

\newcommand*\PropLevel[1]{\ensuremath{P_{#1}}}
\newcommand*\ActionLevel[1]{\ensuremath{A_{#1}}}
\newcommand*\ReqLevel[1]{\ensuremath{R_{#1}}}

Graphplan is planning algorithm introduced by \name{Blum} and \name{Furst}~\cite{blum:1997}.
It is capable of creating partial-order plans.
In contrast to plans of total order, imposing a strict ordering among actions, partially ordered plans weaken the ordering constraint~\cite[chap.~6]{ghallab:2004}.
By allowing sets of actions to be performed in arbitrary order~(including a concurrent execution), the complexity of the planning graph can be reduced because not all orderings have to be explicitly stated.
Across these action sets, however, a total order is maintained.

Graphplan uses a levelled graph~(nodes are partitioned into disjoint sets with edges connecting only nodes in adjacent levels).
Levels alternate between action and propositions levels.
The former contain nodes labelled with a single action.
Similarly, the latter contain nodes labelled with propositions.
Within an action level, actions are independent and may be performed in any order.
The sequence of levels corresponds to the sequence of time steps during plan execution.
Thus, the planning graph can represent partial-order plans using levels to impose a total order.
In addition to concrete actions from the problem domain, action levels also contain \emph{maintenance actions} whose sole purpose is to carry over propositions from one proposition level to the next.
Maintenance actions are treated like any other action.
Planning graphs start with proposition level~\PropLevel{0} containing all propositions constituting the initial state.
Action level~\ActionLevel{i}~(\( i\geq 0 \)) contains all actions whose preconditions are completely satisfied within \PropLevel{i}.
The combined postconditions of all actions in \ActionLevel{i} form the propositions in \PropLevel{i+1}.
Edges from \PropLevel{i} to \ActionLevel{i} identify preconditions that must hold true for an action to be executable in step~\( i \).
From \ActionLevel{i} to \PropLevel{i+1}, edges identify postconditions an action will achieve.
Planning graphs provide information about which actions can be reached~(reachability graph), and thus be executed, via a path from the initial state.

Because actions can interfere with one another, precautions have to be taken to identify such actions and prevent them from interfering with one another when used in a plan.
For this purpose Graphplan makes use of \emph{mutex relations} to identify any incompatibility by connecting mutually exclusive actions with undirected edges.
There are two causes for incompatibility:
\begin{description}[leftmargin=9em, style=nextline]
  \item[competing needs]
    when any precondition of \emph{X} is mutually exclusive with any precondition of \emph{Y}~(one being the negation of the other) so they cannot hold true at the same time
  \item[interference]
    when postconditions of \emph{X} are incompatible with pre- or postconditions of \emph{Y}, or vice versa
\end{description}
The first case directly prevents both actions from being performed concurrently as they cannot be satisfied simultaneously.
The second case identifies actions which cannot be executed independently~(in any order) because one would have effects preventing the other from being executable.
Mutually exclusive propositions are likewise connected with edges in two cases:
\begin{enumerate*}[label=(\arabic*)]
  \item each proposition is mutually exclusive with its negation by definition and
  \item any pair of propositions not achievable simultaneously, \ie all pairs of actions achieving the proposition pair are mutually exclusive.
\end{enumerate*}

Graphplan's planning algorithm is an iterative two-step procedure:
\begin{ienumerate}
  \item search plans when the last proposition layer contains the goal state,
  \item otherwise extend the graph by one action level using the last proposition level as preconditions and propagate mutex relations to the next level.
\end{ienumerate}
The algorithm stops when
\begin{ienumerate}
  \item any plan is found or
  \item no plan can be found and no extension is possible because no action's preconditions are satisfied.
\end{ienumerate}
Furthermore the graph can \emph{level off} when two adjacent proposition levels are identical~(same propositions and mutex relations), in which case further extensions cannot introduce any actions not already present in earlier levels.
Extending a levelled off graph would just carry over every action and mutex relation to the next level.
The plan search uses a level-by-level backward-chaining strategy.
It starts at the very last action level~\ActionLevel{i} and considers all non-mutually-exclusive action combinations achieving the current goals.
For every action combination the combined preconditions become the goals applied to the next action level~\ActionLevel{i-1}.
When some goal cannot be achieved the algorithm backs up and considers another action combination in the previous level.
A plan is found when the current goal, determined in \ActionLevel{0}, is satisfied in \PropLevel{0}~(the initial state).



\paragraph{Evaluation}

Unfortunately, Graphplan cannot be applied to the problem of mashup composition as presented in this thesis.
Firstly, the algorithm requires an initial state to be able to find a plan that can achieve a goal state when applied to this very initial state.
Such information is not necessary for mashup composition when it is solely based on realizing functionalities.
Secondly, Graphplan handles every action as being performed within the same environment~(acting on the same world state).
Such an execution model~(a single world state shared by all actions and, thus, widgets) is applicable to mashups only to some extent.
\gls{IWC} provides a mechanism for widgets to share their state, thereby establishing a kind of global state, although it is not permanent.
It is only effective the very moment messages are published and only if they affect other widget states.
Despite that, each widget has its own independent internal state.
This also allows mashups to contain multiple instances of the same widget, acting separately.
Graphplan cannot distinguish between actions of different instances of the same widget.
This would require an extension to the graph structure and planning algorithm to consider instance information.
Data dependencies, satisfied by publications, can be used as preconditions of individual actions.
