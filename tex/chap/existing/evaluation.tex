\section{Evaluation}

This section evaluates the outlined approaches with respect to each requirement defined in \cref{sec:requirements}.
Because the approaches of \nameref{sec:discovering-sws-using-agents} and \nameref{sec:graphplan} are not directly applicable to mashup composition, their potentials for adaptation to this very problem are considered.
\Cref{tab:approaches-evaluation} shows which requirements can be satisfied by which approach.

\subsection{Functional Requirements}

\begin{ReqEvals}

  \ReqEval{create-mashups}
    \nameref{sec:universal-mashup-composition}, its extension \nameref{sec:task-based-recommendation}, and \nameref{sec:complementary-assistance-mechanisms} are able to create executable mashups.

  \ReqEval{realize-functionalities}
    \nameref{sec:task-based-recommendation} can satisfy this requirement by using a task ontology to describe desired mashups in terms of functionalities.
    \nameref{sec:complementary-assistance-mechanisms} is able to compose mashups for functionalities specified via the \gls{ACE}.

  \ReqEval{widget-model}
    Approaches using the \gls{UCM} are able to use \gls{VWM} widgets as the \gls{UCM} is a generalization of the \gls{VWM}.
    \nameref{sec:complementary-assistance-mechanisms} does not state the supported widget model.

  \ReqEval{complete-mashups}
    Mashups created by \nameref{sec:universal-mashup-composition}, \nameref{sec:task-based-recommendation}, and \nameref{sec:complementary-assistance-mechanisms} are complete in the sense that they satisfy the mashup template, task description, or end-user-defined goals.
    This, however, does not ensure that all used widgets are fully satisfied in their requirements~(given those requirements must be satisfied to create a working mashup).
    This is the case when widget requirements are not considered by authors of mashup templates or task descriptions, neither does \nameref{sec:complementary-assistance-mechanisms} address mashup completeness.

  \ReqEval{detect-incompatibilities}
    Every approach is capable of detecting incompatibilities on the interface level.
    Additionally, \nameref{sec:task-based-recommendation} and \nameref{sec:complementary-assistance-mechanisms} also consider compatibility of functionalities.

  \ReqEval{define-functionalities}
    \nameref{sec:complementary-assistance-mechanisms} allows end-users to specify required functionalities via a dialogue.
    \nameref{sec:task-based-recommendation} can also satisfy this requirement by using a task ontology to describe desired mashups in terms of functionalities.
    This approach, however, does not address less-skilled end users which may complicate the definition of functionalities without a dedicated interface, \eg a dialogue system.

  \ReqEval{feedback}
    Although not mentioned by each approach, \gls{IT} systems in general should be able to provide feedback on failure.
    Reasons for failure can be determined when it is known what causes a failure, \eg unmet requirements or incompatible components.

  \ReqEval{select-mashups}
    \nameref{sec:universal-mashup-composition} and its extension \nameref{sec:task-based-recommendation} do not present all mashups satisfying a template or task definition.
    Instead, the template is present during execution and users can exchange recommended widgets.
    The \gls{ACE} used in \nameref{sec:complementary-assistance-mechanisms} automatically populates the workspace with suitable widgets, effectively using the best mashup in case multiple mashups were possible.
    \nameref{sec:graphplan} and \nameref{sec:htn-planning} can find multiple plans, corresponding to multiple mashups.
    It does not apply to \nameref{sec:discovering-sws-using-agents} as the application is composed during execution.

  \ReqEval{load-mashups}
    Despite not allowing users to select resulting mashups, \nameref{sec:universal-mashup-composition}, \nameref{sec:task-based-recommendation}, and \nameref{sec:complementary-assistance-mechanisms} automatically configure the execution environment with a suitable mashup, in case such a mashup exists.

  \ReqEval{assist-users}
    None of the presented mashup composition approaches addresses end-user assistance in achieving a mashup's functionalities.
    Both \gls{SWS} composition approaches assist users in executing resulting applications.
    This is necessary as web services are not directly invoked by users.
    It requires some intermediate layer responsible for taking inputs, reporting outputs, and guiding execution.

  \ReqEval{execution-steps}
    \nameref{sec:graphplan} and \nameref{sec:htn-planning}, like planning algorithms in general, are used to find a sequence of operations to achieve some goal.
    When \nameref{sec:task-based-recommendation} composes mashups satisfying some composite task, it can use the composite task's structure to determine a sequence of tasks to be performed.

  \ReqEval{prevent-interference}
    The issue of interfering widgets is not addressed by \nameref{sec:universal-mashup-composition}, \nameref{sec:task-based-recommendation}, or \nameref{sec:complementary-assistance-mechanisms}.
    This is understandable for the first approach as its mashups are not bound to a specific use case.
    The second and third approach, however, provide means for specifying a mashup's intent using a task description or functionalities.
    It is not known why interference is not considered an issue or at least ruled out from happening.
    \nameref{sec:graphplan} is specifically designed to prevent action interference during execution.

  \ReqEval{prevent-feedback-loops}
    Like interference, feedback loops are not addressed by \nameref{sec:universal-mashup-composition}.
    It is unknown if this is prevented by the execution environment, \eg with the use of wrappers.

\end{ReqEvals}


\subsection{Non-Functional Requirements}

\begin{ReqEvals}

  \ReqEval{minimal-widgets}
    The discovery and selection process in \nameref{sec:universal-mashup-composition} is bound by a mashup template to only include matching widgets.
    The same applies to \nameref{sec:task-based-recommendation} which uses a mashup's task description as bound on required functionalities.
    \nameref{sec:complementary-assistance-mechanisms}, as well, uses only widgets required by end-user-defined functionalities.
    The planners of \nameref{sec:discovering-sws-using-agents} and \nameref{sec:graphplan} only select web services or actions which are able to partially progress a world state to ultimately achieve a goal state.
    \nameref{sec:htn-planning} considers only tasks given as goal or discovered during task decomposition.

  \ReqEval{efficient-composition}
    Graphplan is proven to be of polynomial size and time, because operators need to be instantiated with objects~\cite[theorem~1]{blum:1997}.
    The complexity of checking if a plan exists for \nameref{sec:htn-planning} is undecidable.
    Restricting tasks to primitive and totally-ordered the complexity can be reduced to polynomial time.
    Omitting any of the two restrictions makes plan existence NP-complete\footnotemark~\cite[table~11.1]{ghallab:2004}.
    All other approaches provide no information about their efficiency.
    However, because of small input sizes~(\eg a few widget templates, tasks, or functionalities) in real-world scenarios, it can be assumed that the complexity of underlying algorithms has no big influence on the overall efficiency.
    Finding suitable widgets in a, possibly, large number of available widgets is a general problem of efficient search~(sorting, filtering, and finding), depending on specific database systems.
    \footnotetext{A decision problem is NP-complete when it is in NP, \ie it can be verified in polynomial time, and every problem in NP can be reduced to this very problem in polynomial time~\cite[sect.~7.4]{sipser:2013}.}

  \ReqEval{ranking-by-complexity}
    Plans found by \nameref{sec:graphplan} and \nameref{sec:htn-planning} provide information to reason about the structure of corresponding mashups.

  \ReqEval{ranking-by-preferences}
    In order to select the most suitable components, \nameref{sec:universal-mashup-composition} and \nameref{sec:task-based-recommendation} perform ranking of each discovered component.
    This ranking of individual components could be applied to entire mashups by combining the individual rankings appropriately.

\end{ReqEvals}

\begin{table}
  \centering
  \caption[Comparison of Existing Approaches Based on Requirements]{%
    Comparison of existing approaches based on requirements from \cref{sec:requirements}.
    Approaches are abbreviated as
    \begin{ienumerate}
      \item[UCM](\nameref{sec:universal-mashup-composition}),
      \item[TBR](\nameref{sec:task-based-recommendation}),
      \item[CAM](\nameref{sec:complementary-assistance-mechanisms}),
      \item[DWS](\nameref{sec:discovering-sws-using-agents}),
      \item[WSC](\nameref{sec:sws-composition}),
      \item[GP](\nameref{sec:graphplan}), and
      \item[HTN](\nameref{sec:htn-planning}).
    \end{ienumerate}
    Requirement satisfaction is either marked as \emph{satisfiable}~(\ReqFullySatisfiable), \emph{not satisfiable}~(\ReqNotSatisfiable), \emph{satisfiable under certain conditions}~(\ReqPartiallySatisfiable), \emph{not applicable}~(\ReqNotApplicable), or \emph{unknown}~(\ReqUnknown).}
  \label{tab:approaches-evaluation}

  \vspace{2ex}

  \begingroup
  \def\arraystretch{1.5}
  \begin{tabular}{c|c:c:c|c:c|c:c}
       Reqs.
    & \makebox[2.2em][c]{UCM}
    & \makebox[2.2em][c]{TBR}
    & \makebox[2.2em][c]{CAM}
    & \makebox[2.2em][c]{DWS}
    & \makebox[2.2em][c]{WSC}
    & \makebox[2.2em][c]{GP}
    & \makebox[2.2em][c]{HTN}

    \\ \hline

       \ReqRef{create-mashups}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable

    \\ \ReqRef{realize-functionalities}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{widget-model}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable

    \\ \ReqRef{complete-mashups}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{detect-incompatibilities}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable

    \\ \ReqRef{define-functionalities}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable

    \\ \ReqRef{feedback}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{select-mashups}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotApplicable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{load-mashups}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable

    \\ \ReqRef{assist-users}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable

    \\ \ReqRef{execution-steps}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{prevent-interference}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqUnknown

    \\ \ReqRef{prevent-feedback-loops}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqUnknown

    \\ \hline

       \ReqRef{minimal-widgets}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{efficient-composition}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqFullySatisfiable
    &  \ReqPartiallySatisfiable

    \\ \ReqRef{ranking-by-complexity}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{ranking-by-preferences}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable

    \\ \hline
  \end{tabular}
  \endgroup
\end{table}
