\chapter{Evaluation}
\label{chap:evaluation}

This section evaluates the prototype implementation presented in \cref{chap:implementation}.
It is shown how the prototype can be applied to the scenario from \cref{sec:scenario}.
Furthermore, the fulfilling of the requirements from \cref{sec:requirements} are evaluated.



\section{Prototype Usage Scenario}

Erin could not manage to find a working mashup to book her holiday in Norway~(\cf scenario from \cref{sec:scenario}) because of to many widget combinations to be manually tried.
How does Erin solve her mashup need, now that she has access to an automated mashup composer?

She starts by creating an empty workspace in Apache Rave and adds the mashup composer widget to the empty mashup.
The required functionalities must be specified in form of a semantic description.
Admittedly, this is a major obstacle for end users that needs to be abstracted.
For example via a dialogue as offered by the \gls{ACE} which uses information about available widgets to carry out flexible conversation~\cite{chowdhury:2013}.
Information about available functionalities is given by the planning service's knowledge base.
In this case Erin must rely in Wanda one more time to help her write the goal description in \gls{Turtle}.
When this is done, Erin submits the planner request~(the planning service is already running).

The composer widget receives a set of three executable plans and lists them according to their rating.
Erin, who has no clue which mashup is most suitable for her needs, picks the very first entry, \ie the mashup with the highest rating.
The execution plan gets created and the mashup is populated with six widgets:
\begin{itemize}
  \item two widgets with a map to select conceptually distinct starting and destination locations,
  \item two widgets to pick conceptually distinct start and end dates,
  \item a widget to find and select hotels, and
  \item a widget to find and select flights.
\end{itemize}
The corresponding instructions, grouped by execution steps, are:
\begin{enumerate}
  \item Perform the following instructions in any order.
    \begin{itemize}
      \item ``Select a starting location.''
      \item ``Select a destination location.''
      \item ``Select a start date.''
      \item ``Select an end date.''
    \end{itemize}
  \item Perform the following instructions in any order.
    \begin{itemize}
      \item ``Select a hotel room.''
      \item ``Select a flight.''
    \end{itemize}
\end{enumerate}
Because Erin can not identify which of the two map widgets is responsible for which location, she requests the corresponding widgets to be highlighted.
Following the instructions, she manages to select a hotel room and flight.



\section{Requirements}

The requirements defined in \cref{sec:requirements} are used as criteria and are considered in terms of how the prototype satisfies them.
\Cref{tab:prototype-evaluation} summarizes how requirements are satisfied by the prototype in comparison with approaches presented in \cref{sec:approaches}.



\subsection{Functional Requirements}

\begin{ReqEvals}

  \ReqEval{create-mashups}
    The planning library produces only executable plans.
    Corresponding mashups are created by the composer widget.

  \ReqEval{realize-functionalities}
    The inclusion of requested functionalities is guaranteed during planning graph construction.
    A planning graph's initial level must contain at least one action per requested functionality.
    The backward-chaining plan search begins by considering all action combinations in the initial level that can achieve all requested functionalities.

  \ReqEval{widget-model}
    The automated planning algorithm is specifically designed for to handle the issues arising in mashups using widgets conforming to \gls{VWM}.

  \ReqEval{complete-mashups}
    Plans found by the backward-chaining search have no unsatisfied requirements for achieving the planning goal.
    Based on plan information the composer widget can create mashups containing all necessary widgets.

  \ReqEval{detect-incompatibilities}
    Incompatibilities are detected and avoided on the semantic level by using semantic description of widget interfaces.
    Functional incompatibilities are prevented during planning as widgets are discovered and selected based on compatibility with some required concepts~(realized functionalities or published properties).
    The composer widget is responsible for detecting incompatibilities regarding the underlying execution model.
    Functionally sound plans may not be executable when their communication, although compatible with respect to data types, cannot work because of incompatible interfaces~(differing property names).
    This kind of incompatibility is specific to the execution model and thus a responsibility of the composer widget.

  \ReqEval{define-functionalities}
    The composer widget provides users with a form to state the required mashup functionality using a semantic description.
    Resulting mashups will conform to this description.

  \ReqEval{feedback}
    The composer widget displays failure messages received from the planning service.
    The planning services catches any exception thrown by the planning library and responds with an error containing the exception message.

  \ReqEval{select-mashups}
    The composer widget lists all executable plans along with their rating.
    Users can select the plan they wish to execute.

  \ReqEval{load-mashups}
    A plan's corresponding mashup is configured by the composer widget using the JavaScript-\gls{API} provided by Apache~Rave.
    It is ensured that a mashup is properly loaded before execution.

  \ReqEval{assist-users}
    When executing mashups, users are assisted with step-by-step instructions of interactions that need to be performed.
    Users can choose the order in which they perform interactions from the same step.
    Target widgets can be highlighted for any instruction.

  \ReqEval{execution-steps}
    The mashup planner produces plans of partially ordered widget actions.
    Planning graph levels correspond to individual execution steps~(total order).
    Interactions within the same step can be executed in arbitrary order.
    Using a mashup's corresponding plan information, it is possible to generate a sequence of execution steps.

  \ReqEval{prevent-interference}
    The effects of \gls{IWC} are considered during planning~(mutual exclusive actions) to avoid widgets from disrupting a mashup's proper functioning.
    Interferences not considered during planning can be avoided by isolating widgets during execution~(only widgets in adjacent levels can communicate)
    Such interferences can occur when users invoke actions not selected during planning but still provided by widgets.
    When strictly following the execution plan, interferences will not occur.

  \ReqEval{prevent-feedback-loops}
    Feedback loops are detected during planning~(also necessary to avoid an infinite regression).
    Actions creating such a loop are discarded and can therefore not be part of any plan.
    As with interference, widget isolation also prevents unforeseen feedback loops.

\end{ReqEvals}


\subsection{Non-Functional Requirements}

\begin{ReqEvals}

  \ReqEval{minimal-widgets}
    The plans contain only actions discovered based on requirements found during planning.
    Resulting mashups use only those widgets providing necessary actions.

  \ReqEval{efficient-composition}
    The planning algorithm is based on Graphplan which offers efficient planning.
    However, the algorithm presented in this thesis diverges from Graphplan because of additional/different requirements.
    The requirement for instance information among widget actions leads to the composition of actions in the same level in order to group actions capable of being executed in the same widget instance.
    This increases the search space by creating more search paths in the planning graph.
    Without action composition, linear complexity in space and quadratic complexity in time is achieved, \ie in this case planning is efficient.
    Action composition, on the other hand, causes the planning algorithm to be exponential in space and time.
    However, depending on real-world use cases exponential complexity may have no negative effect.

  \ReqEval{ranking-by-complexity}
    The ranking of plans is performed on the client-side~(the composer widget).
    The prototype implementation solely uses the information provided with each plan.
    Specifically it favours mashups with fewer widgets, interactions, and execution steps.

  \ReqEval{ranking-by-preferences}
    The prototype implementation ignores any user characteristics that may affect the subjective quality of mashups in terms of execution.

\end{ReqEvals}



\begin{table}
  \centering
  \caption[Comparison of Prototype Implementation and Existing Approaches Based on Requirements]{
    Comparison of prototype implementation and existing approaches based on requirements from \cref{sec:requirements}.
    This table is identical to \cref{tab:approaches-evaluation} except an additional column for the prototype implementation on the very right.
    Approaches are abbreviated as
    \begin{ienumerate}
      \item[UCM](\nameref{sec:universal-mashup-composition}),
      \item[TBR](\nameref{sec:task-based-recommendation}),
      \item[CAM](\nameref{sec:complementary-assistance-mechanisms}),
      \item[DWS](\nameref{sec:discovering-sws-using-agents}),
      \item[WSC](\nameref{sec:sws-composition}),
      \item[GP](\nameref{sec:graphplan}),
      \item[HTN](\nameref{sec:htn-planning}), and
      \item[PI](\nameref{chap:implementation}).
    \end{ienumerate}
    Requirement satisfaction is either marked as \emph{fully satisfiable}~(\ReqFullySatisfiable), \emph{not satisfiable}~(\ReqNotSatisfiable), \emph{satisfiable under certain conditions}~(\ReqPartiallySatisfiable), or \emph{not applicable}~(\ReqNotApplicable).}
  \label{tab:prototype-evaluation}

  \vspace{2ex}

  \begingroup
  \def\arraystretch{1.5}
  \begin{tabular}{c|c:c:c|c:c|c:c|c}
       Reqs.
    & \makebox[2.2em][c]{UCM}
    & \makebox[2.2em][c]{TBR}
    & \makebox[2.2em][c]{CAM}
    & \makebox[2.2em][c]{DWS}
    & \makebox[2.2em][c]{WSC}
    & \makebox[2.2em][c]{GP}
    & \makebox[2.2em][c]{HTN}
    & \makebox[2.2em][c]{PI}

    \\ \hline

       \ReqRef{create-mashups}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{realize-functionalities}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{widget-model}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable

    \\ \ReqRef{complete-mashups}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{detect-incompatibilities}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable

    \\ \ReqRef{define-functionalities}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable

    \\ \ReqRef{feedback}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{select-mashups}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotApplicable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{load-mashups}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable

    \\ \ReqRef{assist-users}
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{execution-steps}
    &  \ReqNotSatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotSatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{prevent-interference}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqFullySatisfiable

    \\ \ReqRef{prevent-feedback-loops}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqUnknown
    &  \ReqFullySatisfiable

    \\ \hline

       \ReqRef{minimal-widgets}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{efficient-composition}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqFullySatisfiable
    &  \ReqPartiallySatisfiable
    &  \ReqPartiallySatisfiable

    \\ \ReqRef{ranking-by-complexity}
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqUnknown
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable

    \\ \ReqRef{ranking-by-preferences}
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqFullySatisfiable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotApplicable
    &  \ReqNotSatisfiable

    \\ \hline
  \end{tabular}
  \endgroup
\end{table}



\summary

The prototype implementation adequately satisfies all functional and most of the non-functional requirements.
Only the non-functional requirements for efficient composition and user-preference-based plan ranking are not fully satisfied.
Because the planning algorithm deviates from Graphplan to address different requirements, Graphplan's algorithmic complexity does not simply apply to the new planning algorithm.
Depending whether actions are composed during planning or not, complexity is either exponential or linear in space.
The planning algorithm is of quadratic or exponential complexity in time, the latter one applying when action composition is used.
Depending on the input for real-world planning scenarios, the exponential complexity may have no effect on actual running time and memory usage.
Plan rating is realized by using only static information given with each plan~(\eg number of interactions or execution steps).
This variant misses the fact that users have different preferences when using \gls{IT} systems.
