\makeatletter

\newcommand\fn[2]{%
  \expandafter\gdef\csname #1\endcsname{\@ifstar{\csname @#1\endcsname}{\csname @@#1\endcsname}}%
  \expandafter\gdef\csname @#1\endcsname{\ensuremath{\mathit{#2}}}%
  \expandafter\gdef\csname @@#1\endcsname ##1{\ensuremath{\mathit{#2}(##1)}}%
}

\fn{preconditions}{pre}
\fn{postconditions}{post}
\fn{realize}{fun}
\fn{instructions}{int}
\fn{effects}{eff}
\fn{precursor}{precursor}
\fn{absent}{absent}
\fn{present}{present}
\fn{match}{match}
\fn{composite}{composite}
\fn{maintain}{maintain}
\fn{actions}{act}
\fn{public}{pub}
\fn{mutexM}{mutex_M}
\fn{mutexW}{mutex_W}

\makeatother



\chapter{Automated Mashup Planner}
\label{chap:planner}

The mashup execution requires partial-order plans to address issues with interfering actions.
Because of this requirement, Graphplan serves as the basis for the mashup planner outlined in this chapter.
It starts with a description of the planning graph structure, followed by its construction, and concludes with the plan search.



\section{Planning Graph Structure}
\label{sec:planning-graph-structure}

To some extent the structure of Graphplan's planning graph can be applied to the planning graph for mashups.
The graph is levelled and alternates between action and requirement layers~(the latter being a generalization of proposition layers).
The terms \emph{level} and \emph{layer} are used interchangeably.
Because of the equivalent structure it is possible to also produce partial-order plans.

However, the graph deviates in a number of ways, to be applicable to the problem of mashup composition.
The most apparent change is that the sequence of layers is reversed regarding the sequence of execution steps.
This inversion arises from the way graphs are constructed.
Based on some target functionality, graphs are constructed in reverse direction with respect to execution order to satisfy requirements of actions in later execution steps.
This strategy is also used by \name{Kambhampati} et al.~\cite{kambhampati:1997}.
Graph construction is outlined in detail in \cref{sec:planning-graph-construction}.
In the following, graph levels are numbered opposite to the sequence of execution steps.
Levels with higher index correspond to earlier steps.
Starting with layer~\ReqLevel{0}, containing nodes labelled with goal functionalities, the graph initially represents a set of all functionality concepts to be realized by a mashup.
The next layer, \ActionLevel{0}, contains nodes labelled with actions realizing functionalities compatible with any functionality in \ReqLevel{0}.
The sequence of layers continues with alternating levels~\ReqLevel{i} and \ActionLevel{i}~(\(i \geq 1\)).
\ReqLevel{i} contains nodes labelled with propositions from the combined preconditions of actions in \ActionLevel{i-1}.
Level~\ActionLevel{i} contains actions having postconditions satisfying requirements in \ReqLevel{i}.
Edges from \ReqLevel{i} to \ActionLevel{i} denote which requirements are satisfied by which actions.
For \ReqLevel{0} and \ActionLevel{0} this means the realization of functionalities.
For \(i \geq 1\), edges are labelled to express an action's postconditions~(when the proposition belongs to the same widget) or publications~(when the proposition belongs to a different widget).
From \ActionLevel{i} to \ReqLevel{i+1}, edges connect actions with their respective preconditions.
\Cref{fig:planning-graph} shows an example planning graph constructed up to a depth of three levels.

\begin{figure}
  \centering
  \includegraphics{fig/graph}
  \caption[Example of a Plannign Graph]{%
    Example of a planning graph of depth 3.
    Bold arrows denote propositions~(presence only) affected by publications~(\gls{IWC}).
    Arcs denote mutex relations.}
  \label{fig:planning-graph}
\end{figure}

Each widget in \( \mathcal{W} \) has a set of actions~(\actions*) and a set of public properties~(\public*).
Actions \( \mathcal{A} \) each have a set of preconditions~(\preconditions*), a set of effects~(\effects*), a set of functionality concepts~(\realize*), and a set of interactions~(\instructions*).
\begin{align}
  \label{eq:widgets} \actions{w}&\subseteq\mathcal{A},\quad
  \public{w}\subseteq\mathcal{P}&\forall w\in\mathcal{W}\\
  \preconditions{a}&\subseteq\mathcal{Q}^*,\quad
  \effects{a}\subseteq\mathcal{Q}^*,\quad
  \realize{a}\subseteq\mathcal{F},\quad
  \instructions{a}\subseteq\mathcal{I}&\forall a\in\mathcal{A}
\end{align}
Preconditions and effects are sets of propositions.
Propositions are related to properties~(a widget's internal state) such that propositions denote the presence or absence of property values.
Given the set of all properties~\( \mathcal{P} \), a proposition is a pairing of a property~\( p \) and a state denoting either the presence or absence of \( p \)'s value.
\begin{subequations}
\begin{align}
  \mathcal{Q}&=\mathcal{P}\times\{\textup{absent},\textup{present}\}\\
  q&=\neg r\iff q=(p,\textup{absent})\land r=(p,\textup{present})
    \qquad q,r\in \mathcal{Q}
\end{align}
\end{subequations}
An action is independent of property~\( p \) when its preconditions do not contain either~\( (p,\textup{absent}) \) or~\( (p,\textup{present}) \).
Because \( \mathcal{Q} \) allows contradicting propositions~(the presence and absence of a property value at the same time), the set has to be further constrained.
Otherwise, an action could cause a cat to be simultaneously dead and alive.%
  \footnote{Alluding to \name{Schrödinger}'s cat~\cite{trimmer:1980}.}
The constrained propositions~\( \mathcal{Q}^* \) state that a property either has a value or no value.
\begin{align}
  \mathcal{Q}^*\subset\mathcal{Q}\iff q\in\mathcal{Q}\land q\in\mathcal{Q}^*\land\neg q\notin\mathcal{Q}^*
\end{align}

The effects do not represent the actual postconditions because this would allow an action to delete a precondition by not including a proposition for a specific property in the set of postconditions.
This behaviour is illogical.
Given a set of preconditions, the minimal world state for an action to be executable is known.
Under the assumption that an action's behaviour is deterministic~(non-deterministic behaviour would require planning under uncertainty as outlined in~\cite[Part~V]{ghallab:2004}), deleting a precondition, and thereby the knowledge about a property's state, is not possible.
When a property value has some state, it will maintain this very state when not affected by an action's behaviour.
However, when an action affects the property value it can only provide a possibly different value or clear the value.
Resulting in either the value's presence or absence respectively.
By using effects, an action can express the changes it will make to the world state when executed.
The postconditions~(\postconditions*) can be derived from effects and preconditions by selecting all propositions given by the effects~(as these will hold true after an execution) and carrying over those preconditions whose properties are not affected by an execution~(by not being mentioned in the effects).
\begin{align}
  \mathcal{Q}^*\supseteq\postconditions{a}=
  \effects{a}\cup\{q|q\in\preconditions{a}\land q,\neg q\notin\effects{a}\}\qquad\forall a\in\mathcal{A}
\end{align}



\subsection*{Mutually Exclusive Actions}

The mutual exclusion of actions is analogous to Graphplan.
However, Graphplan uses mutex relations under the premise of a single world state in which all actions are executed.
This shared state applies to widget only to a certain extent.
For one, distinct widget instances cannot \emph{directly} interfere with internal states of other widgets.
Therefore, Graphplan's mutex relations apply only to actions within the same widget instance.
Such actions are \emph{widget mutex}, denoted by \mutexW*.
\begin{subequations}
\begin{align}
  \mutexW{a_i,a_j}
    &=a_i\neq a_j\land a_i,a_j\in\actions{w}\nonumber\\
    &\qquad\land\lbrack q\in\preconditions{a_i}\land\left(\neg q\in\preconditions{a_j}\lor q\in\effects{a_j}\right)\nonumber\\
    &\qquad\qquad\lor r\in\effects{a_i}\land\neg r\in\effects{a_j}\rbrack\\
  \mutexW{a_i,a_j}&\iff\mutexW{a_j,a_i}
\end{align}
\end{subequations}

Nevertheless, mashups also have the notion of shared state through the usage of \gls{IWC}, realized by publishing any changes to a widget's internal properties.
This allows actions to \emph{indirectly} modify another widget's state when the other widget picks up a published value for one of its properties.
Actions with this kind of interference are called \emph{mashup mutex}, denoted by \mutexM*.
\begin{subequations}
\begin{align}
  \mutexM{a_i,a_j}
    &=a_i\neq a_j\land w_x\neq w_y\nonumber\\
    &\qquad\land a_i\in\actions{w_x}\land a_j\in\actions{w_y}\nonumber\\
    &\qquad\land q\in\effects{a_i}\land\neg q\in\preconditions{a_i}\\
  \mutexM{a_i,a_j}&\iff\mutexM{a_j,a_i}\\
  \mutexM{A}&\iff
    \mutexM{a_i,a_j}
    \;\exists\;a_i,a_j\in A
    \qquad A\subseteq\mathcal{A}
\end{align}
\end{subequations}

These modifications could cause preconditions of some action to no longer hold true thus preventing its execution.
Such conflicting actions cannot be executed in any order and therefore cannot occur in the same execution step, \ie a total ordering must be established by using distinct steps.
This complicates the planning process as mutually exclusive actions cannot be easily labelled without any instance information.
It would require binding actions to widget instances during planing to be able to decide if a \emph{possible} mutual exclusion actually applies~(when both actions share the same widget instance).
Identifying separate instances is necessary to identify mutex relations caused by shared states.

The instance information can be made implicit by requiring that each action within the same level will be executed by a separate widget instance.
Unfortunately this does not allow multiple actions of the same instance in the same execution step.
This can be solved by grouping actions to be executed in the same instance and treating the resulting group as a single action.
A set of actions from the same widget can be composed by combining the preconditions, effects, and functionalities of all actions respectively.
\begin{subequations}
\begin{align}\label{eq:action-composition}
  \composite{A}\in\actions{w}&\iff A\subseteq\actions{w}\land\neg\mutexM{A}\\
  p(\composite{A})&=\bigcup_{a_i\in A}\nolimits p(a_i)
    \qquad\forall p\in\{\preconditions*,\effects*,\realize*,\instructions*\}
\end{align}
\end{subequations}
To avoid the necessity for any mutex relations, the action composition is only possible for independent actions, \ie actions with no mutual exclusion on each other.
The actions in any resulting composite action can therefore be executed in any order.
Defining a total order among these actions is possible but it would add additional distinct actions~(each having an individual total ordering among its constituent actions) causing the graph, and thus the search space, to further grow.

This alone, however, provides no information about instances across multiple execution steps.
Because actions within the same widget instance share the same internal state, two actions \emph{must} share an instance when the preconditions of one action can only be achieved by an action in the very same instance.
This holds true for the absence of property values because absent values cannot be shared via \gls{IWC}.
Non-public properties~(\cref{eq:widgets}), independently of their value state, can also be only affected by widgets themselves as these properties are not exposed to the environment.
An action capable of producing such a state is called \emph{precursor action}.
Formally, action \( a_i \) is precursor of action \( a_j \) when the former achieves all the latter's preconditions which state the absence of property values or use non-public properties.
These preconditions will be identified by \( \preconditions*^* \).
\begin{subequations}
\begin{align}
  \label{eq:precursor-relation}
  \precursor{a_i,a_j}&\iff q\in\postconditions{a_i}\land q\in\preconditions*^*(a_j)
    \qquad a_i,a_j\in\actions{w}\\
  \preconditions*^*(a)&=\{(p,s)|(p,s)\in\preconditions{a}\nonumber\\
                      &\qquad\land\lbrack(s=\textup{absent}\land p\in\public{w})\lor p\notin\public{w}\rbrack\}
\end{align}
\end{subequations}
Any additional preconditions may be achieved by the precursor itself or must be provided by actions of different widget instances.
When some actions are insufficient to be precursors by themselves~(by lacking some required postcondition), it is practicable to create compositions of these actions~(when possible) to gain actions capable of being precursors, as postconditions are combined.
When no combination of actual actions yields a precursor, it is also possible to combine any partial precursor with maintenance actions, in order to get actions with the necessary postconditions.
\begin{subequations}
\begin{align}
  \maintain{w,q}&\in\actions{w}\qquad q\subseteq\mathcal{Q}^*\\
  \preconditions{\maintain{w,q}}&=q\\
  p(\maintain{w,q})&=\varnothing
    \qquad\forall p\in\{\effects*,\realize*,\instructions*\}
\end{align}
\end{subequations}
This allows propositions, not satisfiable by a~(partial) precursor in level~\ActionLevel{i}, to be carried over to \ActionLevel{i+1} and be satisfied by actions in the next level.
Maintenance actions are identical to Graphplan's counterparts with the only difference that Graphplan uses them to propagate state to subsequent levels.
In this case they propagate requirements to the next level.

\example{%
  Action~\( a_1 \) of widget~\( w \) has preconditions \( \preconditions{a_1}=\absent{p_1,p_2,p_3} \).
  Actions \( a_2 \) and \( a_3 \) are partially capable of being precursors with \( \effects{a_2}=\absent{p_1,p_2} \) and \( \effects{a_3}=\absent{p_3} \) respectively.
  However, \( a_2 \) and \( a_3 \) cannot be combined because their preconditions, \( \preconditions{a_2}=\absent{p_4} \) and \( \preconditions{a_3}=\present{p_4} \) respectively, imply competing needs.
  Apparently \( a_1 \) cannot be satisfied, thus no plan exists.
  When combining \( a_2 \) with the maintenance action~\( a_m=\maintain{w,\absent{p_3}} \), \( a_1 \) can be satisfied by \( a_4=\composite{a_2,a_m}\Rightarrow\precursor{a_4,a_1} \).
  The preconditions of \( a_4 \), \( \preconditions{a_4}=\absent{p_3,p_4} \), must then be satisfied by an appropriate action in the next level.
  For example, \( a_3 \) combined with an action clearing the value of \( p_4 \), as \( a_3 \) cannot interfere with \( a_2 \) when occurring in different levels.
}

At this point the need for mutex relations for \emph{actions of same widget instances} is eliminated.
Mutually exclusion of actions sharing state needs to be handled separately.
In contrast to internal states, the global shared state is not ubiquitous as it only appears when any \gls{IWC} happens that leads to a synchronization of widget states.
This means to find any \emph{global mutual exclusions} the places where widget synchronization will happen must be identified.
This is the case for two actions~\( X \) and~\( Y \) in the same level, when \( X\) publishes some value for property~\( p \) and \( Y \) requires the value of \( p \) to be absent.
Because \( Y \) will incorporate any value received for own properties, the absence of \( p \) will no longer hold true, thus \( Y \) cannot be executed after \( X \)~(\( Y \) \emph{before} \( X \) may still be possible).
The order of execution of \( X \) and \( Y \) cannot be arbitrary thus a mutual exclusion between the two exists.
As soon as there are mutually exclusive actions there is a possibility for pairs of postconditions to be mutex when there is no way of achieving them simultaneously.
This must be handled analogous to Graphplan by propagating mutex relations to the next proposition level.
Any mutexes between preconditions must be propagated likewise to the next action level by detecting competing needs.
The propagation must be carried out in direction of execution, in accordance with Graphplan.
However, the graph is constructed in reverse direction, starting from the goals.
This presents the problem that graph extensions may add actions with mutex relations affecting mutexes in later levels as they are derived from information in previous levels~(with respect to execution order).
\name{Kambhampati} et al. present an approach for backward propagation of mutexes~\cite[sect.~4.2]{kambhampati:1997}.
This way, some mutually exclusive actions and propositions can be detected by considering their effects instead of requirements.
However, it is not possible to gather all mutex information because the case of \emph{competing needs} depends on mutual exclusions in earlier execution steps.
The easiest solution would be to determine all mutex relations after each graph extension, assuming that the extension will introduce new mutual exclusions.
This obviously can become costly as the graph grows.



\section{Planning Graph Construction}
\label{sec:planning-graph-construction}

The planning graph construction can be divided into two stages:
\begin{enumerate*}[label=(\arabic*)]
  \item creating an initial graph and
  \item extending an existing graph.
\end{enumerate*}
The initial graph is constructed from a set of goal functionalities represented by level~\ReqLevel{0}.
Actions realizing matching functionalities are placed into level~\ActionLevel{0}.
Extending the graph by adding an action level~\ActionLevel{i+1} is done by treating the preconditions of actions in \ActionLevel{i} as postconditions that need to be achieved.
A graph extension is only necessary when there are preconditions to be satisfied, otherwise the graph as a whole is satisfied.
Precursor actions are necessary to provide instance information across execution steps.
To account for this, the necessity for precursor actions is identified before discovering any actions.
By using the precursor relation defined by \cref{eq:precursor-relation}, it is possible to identify the preconditions requiring a precursor action~(all propositions stating the absence of values as well as propositions on non-public properties).
Using the resulting subset of preconditions, it is trivial to discover and select all actions~(from the same widget) achieving said preconditions.
Remaining unsatisfied preconditions~(expressing the presence of public property values) may be achieved within the same widget instance, requiring the composition of the precursor action with eligible actions.
In general, it is possible to rely on actions publishing values of matching properties to satisfy the presence of respective property values.
This, however, requires additional widget instances in a resulting mashup.



\subsection{Action Discovery and Selection}
\label{sec:action-discovery-and-selection}

The discovery of actions deals with gathering all actions satisfying functional requirements for a particular request.
The selection, on the other hand, is an additional step to filter out any actions not applicable because of non-functional requirements.
This allows the planning graph, and thus the plan search space, to be kept comparably small, when many actions can be discarded.
The functional requirements form the minimal features necessary to compose a working mashup.
For an action there are three distinct functional requirements:
\begin{itemize}
  \item realize matching functionalities to realize mashup functionalities,
  \item publish matching properties to realize data flow within mashups, and
  \item satisfy preconditions to enable execution of other actions.
\end{itemize}

Discovering actions satisfying requested preconditions applies only to the identification of precursor actions.
Because such actions must be in the same instance, the search can be limited to actions of the same widget.
When an action achieves postconditions \emph{identical} to requested preconditions it is a match.
The matching of properties and functionalities follows the procedure outlined in~\cite{pietschmann:2011}~(see \cref{sec:universal-mashup-composition}).
For properties this means that an offered property~\( p_o \) matches a requested property~\( p_r \) when its respective types have an inheritance relation.
Because the requested type presents an upper bound on acceptable values, matching types must be subsumed by the upper bound.
\begin{align}
  \match{p_o,p_r}\iff\mathit{type}(p_o)\sqsubseteq\mathit{type}(p_r)
    \qquad p_o,p_r\in\mathcal{P}
\end{align}
This concept can be directly applied to the matching of functionalities as they can also be described using inheritance.
For example, the functionality \emph{SearchAccommodation} is a generalization~(\ie supertype) of \emph{SearchHotel} and \emph{SearchHomestay}~(each describing a search over distinct kinds of accommodation).
\begin{align}
  \match{{f_o},{f_r}}\iff{f_o}\sqsubseteq{f_r}
    \qquad{f_o},{f_r}\in\mathcal{F}
\end{align}

Non-functional features can be used to describe characteristics affecting mashup usage.
Because mashups are targeted at human users~(instead of, for example, web services to be consumed by machines), this leads to the field of \gls{HCI}.
Evaluating a widget, or its actions, in terms of user interaction requires knowledge about possible mashup users.
When end users themselves request the mashup planning, their preferences and characteristics in terms of computer interaction can be used.
Otherwise, users must provide such information about potential end user groups at whom the mashup is targeted.
The application of non-functional features, however, lies beyond the scope of this thesis.



\subsection{Loop Detection and Prevention}

When extending a graph it may happen that the new action level contains an action used in a previous extension.
Such a scenario can have three causes.
\begin{itemize}
  \item The action was used the first time to satisfy the preconditions of some action or to realize a functionality.
    It is used a second time to satisfy the preconditions of a different action.
  \item The action is part of a feedback loop.
  \item The action is part of a widget-internal cyclic dependency.
\end{itemize}
The first case is harmless as it is owed to the reusability of widgets by being applicable in different scenarios.
The second and third case is more problematic.
Not only because of feedback loops~(\cf \cref{sec:mashup-composition} and \cref{fig:feedback-loop}) but because it could lead to an infinite number of graph extensions.
Within a feedback loop each action is transitively dependent on itself~(receiving a message via the loop causes a publication, triggering another round of the same loop).
An internal cyclic dependency causes a deadlock because two actions wait for one another to clear a property value.
In the planning graph, the second and third case manifests itself as a path along the edges between action and proposition levels, as these edges denote dependencies between actions.
\Cref{fig:graph-feedback-loop} illustrates all three cases when no precautions are taken to prevent loops.

\begin{figure}
  \centering
  \includegraphics{fig/graph-feedback-loop}
  \caption[Planning Graph containing Cyclic Dependencies]{
    Example of a planning graph with cyclic dependencies.
    The top path contains \( a_2 \) independent of its other occurrences.
    The middle path causes a feedback loop~(\( a_2\rightarrow c_1\rightarrow a_2 \)).
    The bottom path causes a deadlock~(containing only \( a_1 \) because it maintains one of its precondition).
    Dashed arcs denote the first action recurrence in each case.}
  \label{fig:graph-feedback-loop}
\end{figure}

When the same action appears multiple times on a dependency path, a feedback loop or cyclic dependency exists.
This information can be used to determine if an action, when added to the graph, would cause a transitive dependency on itself in a previous action level.
Because the paths need to be traced up to level~\ActionLevel{0}, this test becomes costly as the planning graph grows in depth.
There are two possible optimizations to speed up this test.
\begin{itemize}
  \item Storing if an action occurs in any previous level thereby eliminating unnecessary searches when an action was never used earlier during graph construction.
  \item Storing for each action in every level~\ActionLevel{k} which actions in \ActionLevel{i}~(\( i<k \)) are dependent, resulting in a plain action lookup.
\end{itemize}



\subsection{Action Composition}
\label{sec:action-composition}

The composition of actions~(\cref{eq:action-composition}) produces a single action having the combined preconditions, effects, and functionalities of its constituting actions.
Only actions within the same action level are eligible for composition.
Therefore, the composition can be applied after all actions have been selected for a new action level during graph extension.
An action combination is a subset of all actions in some action level.
Because it may be preferable to record all possible combinations~(in order to get all action constellations feasible within the same widget instance) it is necessary to consider each of these combinations.
The product set~\( \mathcal{P}(S) \) of some set~\( S \) represents the set of all combinations of members in \( S \).
The cardinality of a product set is \( |\mathcal{P}(S)|=2^{|S|} \), given \( S \) is finite.
This can be easily understood when describing each member of the power set using a binary string containing \( n=|S| \) bits~(each bit indicating the membership of one element from \( S \) in some of its subsets).
The binary string can represent \( 2^n \) distinct states, hence \( 2^n \) different combinations of elements in \( S \).
For small \( n \), considering all combinations is practicable.
However, as \( n \) becomes greater, the number of combinations grows exponentially.

But not all combinations have to be tried.
Single-action combinations are trivial, although their number is negligible for greater \( n \)~(\( n\ll2^n\iff 0\ll n \)).
Because some actions cannot be combined by definition, namely actions of different widgets and mutually exclusive actions, many combinations can be eliminated without checking them at all.
Firstly, the actions can be partitioned into disjoint sets, one per distinct widget.
The resulting smaller sets each have a power set of smaller cardinality~(\( 2^m+2^n\ll2^{m+n}\iff0\ll m+n \)), hence fewer combinations to test.
Secondly, when a set of actions contains a pair of mutually exclusive actions, all of its supersets will also contain this mutex relation.
Because these supersets cannot produce a combined action, they can be ignored.
To make use of this information the combinations must be considered in a fixed order, beginning with the smallest followed by the next larger combinations.
When a set of actions cannot be combined, its supersets can be easily excluded from testing as they have not yet been considered.
\Cref{alg:ordered-power-set} provides a procedure to generate a power set as a sequence of sets ordered by cardinality.

\begin{algorithm}
  \caption[Creation of Power Sets as Ordered Sequences of Subsets]{%
    Generate a subset of a set's power set as a sequence ordered by ascending cardinality.
    Allows for the exclusion of individual subsets and their supersets.}
  \label{alg:ordered-power-set}
  \begin{algorithmic}[1]
    \Require
      \Statex \( S \) : the input set
      \Statex \( [\mathit{include}:\mathcal{P}(S)\rightarrow\mathbb{B}] \) : a predicate function to process sets and decide if supersets of some subset of \( S \) should be considered in further processing
    \Statex
    \State \( Q\gets(\varnothing) \)
    \Repeat
      \State \( s\gets\mathit{head}(Q) \)
      \If{\( \mathit{include}(s) \)}
        \ForAll{\( e\in S\setminus s \)}
          \State \( Q\gets\mathit{append}(Q,s\cup\{e\}) \)
        \EndFor
      \EndIf
      \State \( Q\gets\mathit{tail}(Q) \)
    \Until{\( Q \) is empty}
  \end{algorithmic}
\end{algorithm}



\section{Plan Search}
\label{sec:plan-search}

The search for plans in the planning graph uses the backward-chaining search algorithm of Graphplan~\cite[sect.~3.2]{blum:1997}.
This algorithm works recursively on the graph structure.
Using a set of current goals, the algorithm considers each action combination without any mutual exclusions in \ActionLevel{i} satisfying all current goals.
For level~\ReqLevel{0} the goal functionalities constitute the current goal.
The preconditions of an action combination become the next goals and the algorithm recursively operates on level~\ActionLevel{i+1}.
The algorithm backtracks to the previous level when the goal cannot be satisfied or no more combinations are available.
In the previous level the next action combination, when available, is tried.
When there are no more combinations in \ActionLevel{0}, the search halts.
A plan is found when an action combination has no preconditions~(the actions are satisfied by definition).
Resulting plans are subgraphs of the planning graph.
They contain all goal functionalities, have no actions with mutex relations in the same level, and each dependency path ends at an action without preconditions.
\Cref{fig:graph-with-plan} shows a planning graph with highlighted plan.

\begin{figure}
  \centering
  \includegraphics{fig/graph-with-plan}
  \caption[Example of a Plannign Graph with a Found Plan]{%
    The planning graph from \cref{fig:planning-graph} with a found plan.
    Faint elements are not part of the plan.}
  \label{fig:graph-with-plan}
\end{figure}

Graphplan also describes an optimization to prevent unnecessary searches on paths which cannot be satisfied.
This is the case when paths end with an action in the very last level, having unsatisfied preconditions because the graph contains no further action level to satisfy these preconditions.
Before searching the graph for a goal, the algorithm can check if it cannot be achieved in the current level, in which case the algorithm backs up immediately.
The information about non-achievable goal subsets is recorded for each graph level.

Another optimization would be to determine which actions are reachable in a potential plan during execution.
Actions are always executable when they have no preconditions.
Starting at the very last action level, this information can be propagated to the very first action level to determine each action's potential reachability.
Actions solely depending on non-reachable actions are not reachable either.
However, when any of their dependencies are executable they are \emph{potentially} executable.
Using this information it is possible to eliminate entire search paths as early as possible.



\subsection{Identifying Widget Instances}
\label{sec:identifying-widget-instances}
\here{identify-widget-instances}

The number of widget instances is not directly given by the graph structure.
It can, however, be easily identified when each widget will occur with only one instance in the mashup.
In this case one needs to count the number of distinct widgets across all actions.
However, it is unknown if this is the case without analysing the graph.
Multiple instances are caused by distinct actions of the same widget in the same level, as their execution is required to take place in separate instances.
Using the information on precursor actions, it is possible to identify instances across graph levels.
A problem arises when an action in \ActionLevel{i} is marked as the precursor of multiple distinct actions in \ActionLevel{i-1}.
Because a single precursor action cannot be used in separate instances, it must be duplicated to assign an individual precursor to each dependent action.
When this precursor action itself requires a precursor, this subsequent precursor must be split equally.
This process must be propagated to every subsequent precursor action, possibly to the very last action level.
The resulting graph~(with possibly duplicate actions per level) contains precursors only in a one-to-one relation with their respective dependent actions.



\subsection{Plan Rating}

In addition to the selection of suitable actions during graph extension, extracted plans can be rated to determine their relevance for end users.
The rating is entirely based on non-functional properties as plans, by definition, contain only actions satisfying functional requirements.
Because non-functional features~(as outlined in \cref{sec:action-discovery-and-selection}) can be satisfied by multiple widgets with differing quality, a scoring of widgets can be applied to evaluate their suitability.
Beside that, a plan can be rated in its entirety by considering the total number of interactions~(with respect to multiple widget instances as described \where{identify-widget-instances}) as a measure for the number of steps users have to perform.
Although favouring fewer interactions does not result in a better mashup in principle.
A smaller mashup may be more difficult for users to work with when input or output are not suitable for their needs.
This issue can be circumvented by first ranking the plans based on non-functional features and rating equally ranked plans according to their size, favouring smaller plans.



\section{Computational Complexity}

Graphplan is of polynomial time and space~(\( \BigO(mn^k) \)) because \( m \) operators can be instantiated with \( n \) objects by substituting up to \( k \) variables~\cite[theorem~1]{blum:1997}.
When operators are already fully instantiated, \ie there are no variables~(\( k=0 \)), complexity is linear in space and time~(\( \BigO(mn^k)|_{k=0}=\BigO(m) \)).
This is also the case for the derived planning algorithm, presented in this chapter, relying on widget actions which have no variables.
The upper bound for the number of actions per action level is \( \BigO(n) \), given \( n \) available widget actions.
The number of propositions per proposition level is upper-bounded by \( \BigO(np) \) where \( p \) is the largest number of propositions for any widget action.
When action composition is used, the upper bounds change to \( \BigO(2^n) \) and \( \BigO(2^np) \) respectively.
The identification of mutual exclusions between actions is \( \BigO(n^2) \) as all action pairings need to be considered, it is \( \BigO(2^n) \) when action composition is applied.
This means the presented planning algorithm is of exponential space and time in construction.

Given a planning graph of depth \( d \), finding a plan has complexity \( \BigO((2^n!)^d) \).
This is evident when considering that for every action in any of the first \( d-1 \) levels, there are up to \( (2^n!) \) combinations of satisfying actions in the next level.
Conducting the search up to the very last level may require traversing every search path until the very first plan, or plan at all, is found.

The exponential and factorial upper bound, however, is unlikely to manifest itself in real-world scenarios.
The term \( 2^n \) assumes that all action combination are possible for composition.
Firstly, only a subset of widget actions will be applicable to some planning problem in terms of compatible functionalities and properties.
Of those functionally satisfying actions some actions may be ignored when not suitable for a particular end user group.
Secondly, as stated \cref{sec:action-composition}, certain action combinations can be ruled out.
Therefore, \( 2^m \) is more realistic, where \( m \) is the average number of actions per widget~(\( m\leq n \)).
The worst case would be a single widget with \( n\gg0 \) actions.
Furthermore, mutual exclusions can be used to eliminate impossible combinations of actions of the same widget.
The plan search does not have to traverse every possible path multiple times.
Fruitless paths can be eliminated by memoizing paths ending in unsatisfied actions during search or via a separate processing step before search.
These factors are hidden by \BigO-notation~\cite[p.~276]{sipser:2013}.



\summary

This chapter presented a new planning algorithm derived from Graphplan.
This new algorithm, along with its planning graph structure, is required because Graphplan cannot be directly applied to the requirements of mashup composition.
Planning graph construction starts with a mashup's goal functionalities and discovers actions realizing compatible functionalities.
Any unsatisfied actions discovered during planning needs to be satisfied via other actions.
The internal state of widgets, described via properties, serves as the requirement that needs to be satisfied in order for actions to be executable.
Properties can either be satisfied by actions of the same widget~(precursor actions) or by actions of arbitrary widgets~(also additional instances of the same widget).
Precursor actions can directly affect a widget's internal state and are required for satisfying non-public properties or properties requiring the absence of their value.
All other cases rely on \gls{IWC} to provide property values.
Analogous to Graphplan, mutual exclusions are used to identify actions which cannot be executed in arbitrary order, because of interferences occurring when executed in some sequence.
Among such actions a total ordering is established with the use of adjacent graph levels.
Within each level actions may be executed in any order.
This allows resulting plans to be partially ordered.

Graphplan's backward-chaining plan search can be directly applied to this graph structure as it is essentially the same.
The only exceptions being the
\begin{ienumerate}
  \item that initial level is constructed from functionalities rather than a goal state and
  \item that all search paths need to end in satisfied actions~(in any level) rather than an initial world state.
\end{ienumerate}

The new algorithm is of linear or exponential space in the number of widget actions.
The former applies when no action composition is used, in contrast to the latter because of additional actions per level.
Planning graph construction is of quadratic or exponential time in the number of widgets actions~(depending whether action composition is used) as mutual exclusions require the considering of every action pairing.
Plan search is of factorial time because of the planning graph's branching search paths.
This worst case complexity, however, may not have effects on a real-world scenario where the number of applicable actions is likely to be rather small compared to possibly large number of available actions.
