\chapter{Conclusion and Future Work}
\label{chap:conclusion}

This thesis presented a solution to the automated composition of \gls{UI} mashups through planning using widgets conforming to the \gls{VWM}.
Based on \name{Blum}'s Graphplan, a new planning algorithm was developed, addressing the requirements imposed by the end-user-provided intent and the execution of resulting mashups.
End users express their need for mashups in terms of functionalities to be realized, an approach taken from the task-based recommendation presented by \name{Tietze} et al.
Widgets describe their interface in terms of actions and an internal state~(divided into public and non-public properties) using semantic data.
For this purpose an ontology was designed based on concepts from the universal widget modelling proposed by \name{Pietschmann} et al.

A prototype implementation consisting of a planner library, web service, and composer widget was created, realizing the concept from \cref{chap:concept}.
The library is designed to be reused in other software and provides an implementation of the planning algorithm from \cref{chap:planner}.
The web service exposes an endpoint for consuming the planning algorithm.
Client-server communication uses \gls{JSON} for data serialization.
The structure of request and response data is properly defined using \gls{JSON}~Schema, including a schema for plans produced by the planner.
Users interact with the composer widget to state the intended mashup functionalities and consume the web service.
Resulting plans are rated and presented to users for selection.
Selecting a plan causes the composer widget to configure the respective mashup using Apache Rave's \gls{API}.
This causes a tight coupling between composer widget and Apache Rave.
Users are assisted in executing mashups with a sequence of instructions provided by each widget's semantic description.
Instructions must be followed as stated for a mashup's functionalities to be achieved.
Effects caused by deviations from a planned execution cannot be avoided.
Their effects on the mashup through \gls{IWC}, however, are prevented by configuring the execution environment as execution progresses to filter out unexpected data flow.

The defined requirements are adequately satisfied by the prototype.
Only the non-functional requirements for efficient composition and user-preference-based plan ranking are not fully satisfied, providing issues to be addressed in future work.
Because the planning algorithm deviates from Graphplan to address different requirements, Graphplan's algorithmic complexity does not simply apply to the new planning algorithm.
Depending if actions are composed during planning or not, complexity is either exponential or linear in space.
The planning algorithm is of quadratic complexity in time and exponential when action composition is applied.
Depending on the input of real-world planning scenarios, the exponential complexity may have no effect on actual running time and memory usage.
Plan rating is realized by using only static information available with each plan~(\eg number of interactions or widget instances).
This variant misses the fact that users have different preferences when using \gls{IT} systems.
To make use of user characteristics, widgets must describe their interface in terms of accessibility in a quantified manner to allow comparison of requirements and candidates.



\section*{Open Issues to be Addressed in Future Work}

The specification of goal functionalities requires users to write a semantic description~(\eg in \gls{Turtle}).
This is an unnecessary large obstacle for average users.
Therefore, a dialogue system is required which has knowledge about available functionalities and can conduct a conversation to determine end user needs~(\cf\cref{sec:complementary-assistance-mechanisms}).

The planning algorithm in its current form treats all goal functionalities to be realized in the very last execution step.
It does so by creating the very first planning graph level with actions realizing any goal functionality.
With this strategy, the algorithm can only produce plans which achieve all functionality in the same~(last) execution step.
This, however, is an artificial constraint on mashup execution as functionalities can certainly be achieved in middle of execution.
There are two cases for this to be required:
\begin{ienumerate}
  \item end users want functionalities to be achievable in a specific order or
  \item some functionalities cannot be executed in the same execution step.
\end{ienumerate}
The first case is debatable given that end users may not have sufficient knowledge about a domain and cannot provide a sensible order.
The planner may not be able to produce plans achieving functionalities in a specific sequence, whereas some plans may be possible when functionalities were allowed to be achieved in any order.
The second case applies to mutually exclusive actions achieving some goal functionalities.
In this situation, the planner must be able to impose a total order among these functionalities using a total order among their achieving actions.
When this strategy is pursued the backward-chaining plan search must be modified, as it currently selects any action combination in the initial level achieving all goal functionalities.
Once functionalities are spread over multiple graph levels they can no longer be treated as achievable as a whole in a single graph level.

The mashup planner may select a single action~\( a_0 \) to satisfy multiple actions~\( A=\{a_1,\dots,a_n\} \) from different widgets with the same data~(through publication) in the same execution step.
Corresponding plans will state that actions~\( A \) depend on action~\( a_0 \).
A naive translation into a mashup would create a single widget instance to provide \( a_0 \).
This causes all corresponding widgets of \( A \) to receive the very same data published by \( a_0 \).
In some uses cases this can be useful, \eg in the travel scenario from \cref{sec:scenario} it would be ideal when the widgets for hotel and flight booking receive the same start and end date.
Otherwise, users would have to provide the same date multiple times, thereby increasing the risk for mistakes.
This effectively duplicates \( a_0 \) in multiple widget instances, one per satisfied action in \( A \).
At the same time, this configuration may be desired to allow the input if independent values where necessary.
Without knowledge about a particular use case, it is impossible to automatically determine the right configuration, given that it may needs to be determined for more than just one publishing action.
Mashup orchestration deals with this problem of determining intended and possible data flow~\cite[sect.~4.3]{soylu:2012}~\cite{chudnovskyy:2013}.
To solve this prior to mashup execution, end users could be presented with a graph illustrating a mashup's data flow.
This graph would be similar to the planning graph which encodes the necessary data flow to enable the execution of actions.
The graph must be properly documented, \ie it is comprehensible which actions require which data to be provided by which actions.
When this is the case, end users should be able to modify the data flow by splitting actions where necessary, thereby creating additional widget instances.
This process is analogous to precursor splitting outlined in \cref{sec:identifying-widget-instances}.
