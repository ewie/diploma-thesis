LATEXMK = latexmk

GHOSTSCRIPT = gs

DOCUMENT = document

YYYYMMDD = $(shell date +%Y%m%d)

DOCNAME = CobaltDiplomaThesis

DRAFT = $(YYYYMMDD)-DRF-$(DOCNAME)
PUB = $(YYYYMMDD)-PUB-$(DOCNAME)

.PHONY: pdf pdf-gray draft pub fig clean clean-aux clean-fig

pdf: fig
	$(LATEXMK) -pdf -pdflatex="pdflatex --shell-escape %O %S" -bibtex tex/$(DOCUMENT)

pdf-gray: pdf
	$(GHOSTSCRIPT) \
	 -sOutputFile=grayscale.pdf \
	 -sDEVICE=pdfwrite \
	 -sColorConversionStrategy=Gray \
	 -dProcessColorModel=/DeviceGray \
	 -dCompatibilityLevel=1.4 \
	 -dNOPAUSE \
	 -dBATCH \
	 $(DOCUMENT).pdf

draft: pdf
	cp $(DOCUMENT).pdf $(DRAFT).pdf

pub: pdf
	cp $(DOCUMENT).pdf $(PUB).pdf

fig:
	make -f Makefile -C fig pdf

clean-fig:
	make -f Makefile -C fig clean

clean-aux:
	$(LATEXMK) -C tex/$(DOCUMENT)
	rm -f document.*

clean: clean-aux clean-fig
