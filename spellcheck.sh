#!/bin/sh

mkdir -p temp

for file in $(find tex/chap/ -name '*.tex')
do
  echo $file "->" temp/$file
  mkdir -p temp/$(dirname $file)
  sed 's/%.*$//g' $file > temp/$file
done

java -jar ~/LanguageTool/languagetool-commandline.jar --language en --encoding utf-8 --disable WHITESPACE_RULE --recursive temp/ | less

rm -rf temp
