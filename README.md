# Diploma Thesis

LaTeX source of my Diploma Thesis titled _Semantic-based Planning of User Interface Mashups_ published 22 September 2014 ([20140922-PUB-CobaltDiplomaThesis.pdf](20140922-PUB-CobaltDiplomaThesis.pdf)).

The projects source code can be found at http://gitlab.com/ewie/cobalt.


## Building

```
make pdf
```
